# Orbicular router
Heuristics for approximate solution of linear complexity for the Traveling Salesman Problem.

## Prerequisites
* Unity  
Basically, any version should work since no exotic API is used, Unity is only used for ease of visualization.

## Run
* Start Unity and open Scene.unity
* In hierarchy window select object named 'TravelingSalesman'
* In the inspector set up parameters and press 'Build' button
* Press 'Heuristic auto params Orbicular' button to solve the problem
* [Press any of buttons under 'Solvers' section to use other algorithms]

## Limitations and future improvements
Currently, algorithm performs poor when cities are not generated inside a circle, it's a thing being worked on...
Algorithm works for cities in 2D space, it could be generalized to nD though.

