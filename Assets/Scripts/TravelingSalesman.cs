using UnityEngine;
using System.Collections.Generic;

public class TravelingSalesman : MonoBehaviour
{
	public enum AlgorithmEnum
	{
		BruteForce,
		NearestNeighbour,
		HeldKarp,
		Orbicular,
		AutoParamsOrbicular,
		HeuristicAutoParamsOrbicular
	}

	public enum DistanceCalculatorEnum
	{
		EuclidDistance,
		SumDistance,
		MaxDistance
	}

	public enum OriginCalculatorEnum
	{
		WeightedCenter,
		BoundingBoxCenter,
		FurthestCornerOrigin
	}

	private const int RETRY_COUNT = 10;

	// World generation
	[HideInInspector]
	public int numberOfCities = 5;
	[HideInInspector]
	public int seed = 0;
	[HideInInspector]
	public float minDistance = 1f;
	[HideInInspector]

	// World dimensions
	public float worldWidth = 20f;
	[HideInInspector]
	public float worldHeight = 20f;

	// Routing settings
	[HideInInspector]
	public DistanceCalculatorEnum distanceCalculatorSetting;

	[HideInInspector]
	public OriginCalculatorEnum originCalculatorSetting;

	public List<float> orbicularRadiuses;

	private World world;

	public void BuildWorld(bool inCircle, int seed) {
		DestroyWorld();

		world = new World();
		Random.seed = seed;

		while (world.CitiesCount < numberOfCities) {
			int retryCount = RETRY_COUNT;
			for (; retryCount > 0; --retryCount) {
				Vector2 random2;
				if (inCircle)
				{
					random2 = Random.insideUnitCircle;
				}
				else
				{
					random2 = new Vector2(Random.value, Random.value);
					random2 *= 2f;
					random2 -= new Vector2(1f, 1f);
				}
				Vector3 position = new Vector3(random2.x * worldWidth / 2, 0f, random2.y * worldHeight / 2);
				if (IsPositionEligible(position)) {
					world.BuildCity(position);
					break;
                }
			}
			if (retryCount <= 0) {
				Debug.LogError("Retry count exceeded after " + world.CitiesCount + " cities generated. Widen the area or shorten minimum distance.");
				break;
			}
		}
	}

	public void DestroyWorld() {
		if (world != null) {
			world.Destroy();
			world = null;
		}
	}

	public bool HasWorld() {
		return world != null;
	}

	public int BuildCitiesCount() {
		return HasWorld() ? world.CitiesCount : -1;
	}

	public void Solve(AlgorithmEnum algorithmEnum)
	{
		Solve(algorithmEnum, distanceCalculatorSetting, originCalculatorSetting);
	}

	public void Solve(AlgorithmEnum algorithmEnum, DistanceCalculatorEnum distanceEnum, OriginCalculatorEnum originEnum)
	{
		var distanceCalculator = GetDistanceCalculator(distanceEnum);
		var originCalculator = GetOriginCalculator(originEnum);
		var router = GetRouter(world.Cities, algorithmEnum, distanceCalculator, originCalculator);

		Result result = Result.Build(router, distanceCalculator);
		world.BuildRoads(result);
		UnityEngine.Debug.Log("Routing distance: " + result.Distance);
		UnityEngine.Debug.Log("Execution time: " + result.ExecutionTime + "ms");

		List<float> radiuses = null;
		if (router as AutoParamsOrbicularRouter != null)
		{
			radiuses = (router as AutoParamsOrbicularRouter).Radiuses;
		}
		if (router as HeuristicAutoParamsOrbicularRouter != null)
		{
			radiuses = (router as HeuristicAutoParamsOrbicularRouter).Radiuses;
		}
		if (radiuses != null)
		{
			LogRadiuses(radiuses);
		}
	}

	public static ICityDistance GetDistanceCalculator(DistanceCalculatorEnum type)
	{
		switch (type)
		{
		case DistanceCalculatorEnum.EuclidDistance:
			return EuclidDistanceCalculator.GetInstance();
		case DistanceCalculatorEnum.MaxDistance:
			return MaxDistanceCalculator.GetInstance();
		case DistanceCalculatorEnum.SumDistance:
			return SumDistanceCalculator.GetInstance();
		}
		return null;
	}

	private IOrigin GetOriginCalculator(OriginCalculatorEnum type)
	{
		switch (type)
		{
		case OriginCalculatorEnum.BoundingBoxCenter:
			return new BoundingBoxCenterOrigin();
		case OriginCalculatorEnum.FurthestCornerOrigin:
			return new FurthestCornerOrigin();
		case OriginCalculatorEnum.WeightedCenter:
			return new WeightedCenterOrigin();
		}
		return null;
	}

	private AbstractRouter GetRouter(List<ICity> cities, AlgorithmEnum type, ICityDistance distanceCalculator, IOrigin originCalculator)
	{
		switch (type)
		{
		case AlgorithmEnum.BruteForce:
			return new BruteForceRouter(cities, distanceCalculator);
		case AlgorithmEnum.NearestNeighbour:
			return new NearestNeighbourRouter(cities, distanceCalculator);
		case AlgorithmEnum.HeldKarp:
			return new HeldKarpRouter(cities, distanceCalculator);
		case AlgorithmEnum.Orbicular:
			return new OrbicularRouter(cities, distanceCalculator, originCalculator, orbicularRadiuses);
		case AlgorithmEnum.AutoParamsOrbicular:
			return new AutoParamsOrbicularRouter(cities, distanceCalculator, originCalculator);
		case AlgorithmEnum.HeuristicAutoParamsOrbicular:
			return new HeuristicAutoParamsOrbicularRouter(cities, distanceCalculator, originCalculator);
		}
		return null;
	}

	private void LogRadiuses(List<float> radiuses) {
		string radiusesString = "Radiuses found: ";
		for (int i = 0; i < radiuses.Count; ++i) {
			radiusesString += radiuses[i];
			if (i != radiuses.Count - 1) {
				radiusesString += ", ";
			}
		}
		UnityEngine.Debug.Log(radiusesString);
	}

	private bool IsPositionEligible(Vector3 position) {
		if (minDistance <= 0) return true;

		float closestDistance = float.MaxValue;
		ICityDistance distanceCalculator = EuclidDistanceCalculator.GetInstance();
		for (int i = 0; i < world.CitiesCount; ++i) {
			float distance = distanceCalculator.GetDistance(position, world.Cities[i].GetPosition());
			if (distance < closestDistance) {
				closestDistance = distance;
				if (closestDistance < minDistance) break;
			}
		}

		return closestDistance > minDistance;
    }

	public void UpdateWorldDimensions() {
		if (world != null) {
			world.SetWorldDimensions(worldWidth, worldHeight);
		}
	}
}
