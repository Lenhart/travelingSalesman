﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PerformanceAnalyzer))]
public class PerformanceAnalyzerInspector : Editor {
	public override void OnInspectorGUI() {
		var analyzer = target as PerformanceAnalyzer;

		DrawDefaultInspector();

		if (GUILayout.Button("Analyze performance")) {
			var performanceAnalysis = BuildHeader();
			var randomSeed = analyzer.startingRandomSeed;
			foreach (var worldDimension in analyzer.worldDimensions) {
				foreach (var numberOfCities in analyzer.numberOfCities) {
					foreach (var citiesInCircle in analyzer.citiesInCircle) {
						var cities = BuildCities(worldDimension.x, worldDimension.y, numberOfCities, citiesInCircle, randomSeed);
						foreach (var distanceCalculatorEnum in analyzer.distanceCalculators) {
							var distanceCalculator = TravelingSalesman.GetDistanceCalculator(distanceCalculatorEnum);
							bool bruteForce = analyzer.AnalyzeBruteForce == 0
								|| analyzer.AnalyzeBruteForce >= numberOfCities;
							bool heldKarp = analyzer.AnalyzeHeldKarp == 0
								|| analyzer.AnalyzeHeldKarp >= numberOfCities;
							bool nearestNeighbour = analyzer.AnalyzeNearestNeighbour == 0
								|| analyzer.AnalyzeNearestNeighbour >= numberOfCities;
							bool autoParamsOrbicular = analyzer.AnalyzeAutoParamsOrbicular == 0
								|| analyzer.AnalyzeAutoParamsOrbicular >= numberOfCities;
							bool heuristicOrbicular = analyzer.AnalyzeHeuristicOrbicular == 0
								|| analyzer.AnalyzeHeuristicOrbicular >= numberOfCities;
							var newResults = BuildResults(cities, distanceCalculator, bruteForce, heldKarp,
								nearestNeighbour, autoParamsOrbicular, heuristicOrbicular);

							performanceAnalysis = AnalyzeResults(performanceAnalysis, worldDimension.x, worldDimension.y, numberOfCities,
								citiesInCircle, randomSeed, distanceCalculator, bruteForce, heldKarp, nearestNeighbour,
								autoParamsOrbicular, heuristicOrbicular, newResults);

							++randomSeed;
						}
					}
				}
			}
			Debug.Log(performanceAnalysis);
		}
	}

	private string BuildHeader() {
		return "Performance analysis:\nValues in brackets are always (distance, time)\nWidth | Height | Cities | In circle | Random seed | Distance calculator | BruteForce | HeldKarp | NearestNeighbour Best / Average / Worst | AutoParamsOrbiclar Weighted / Bounded / Furthest | HeuristicOrbicular Weighted / Bounded / Furthest";
	}

	private List<ICity> BuildCities(float worldWidth, float worldHeight, int numberOfCities, bool inCircle, int randomSeed) {
		var cities = new List<ICity>(numberOfCities);

		float halfWidth = worldWidth / 2f;
		float halfHeight = worldHeight / 2f;
		Random.seed = randomSeed;
		for (int i = 0; i < numberOfCities; ++i) {
			Vector3 position;
			if (inCircle) {
				var random = Random.insideUnitCircle;
				position = new Vector3(random.x * halfWidth, 0f, random.y * halfHeight);
			}
			else {
				var x = (Random.value * 2f - 1f) * halfWidth;
				var z = (Random.value * 2f - 1f) * halfHeight;
				position = new Vector3(x, 0f, z);
			}

			var city = new PerformanceAnalyzer.City(position);
			cities.Add(city);
		}

		return cities;
	}

	private List<List<Result>> BuildResults(List<ICity> cities, ICityDistance distanceCalculator, bool bruteForce, bool heldKarp,
			bool nearestNeighbour, bool autoParamsOrbicular, bool heuristicOrbicular) {
		var result = new List<List<Result>>(5);

		if (bruteForce) {
			var router = new BruteForceRouter(cities, distanceCalculator);
			result.Add(BuildResults(router, distanceCalculator, 1));
		}
		if (heldKarp) {
			var router = new HeldKarpRouter(cities, distanceCalculator);
			result.Add(BuildResults(router, distanceCalculator, 1));
		}
		if (nearestNeighbour) {
			var router = new NearestNeighbourRouter(cities, distanceCalculator);
			result.Add(BuildResults(router, distanceCalculator, PerformanceAnalyzer.NEAREST_NEIGHBOUR_TIMES));
		}
		if (autoParamsOrbicular) {
			var routers = new List<AbstractRouter>{
				new AutoParamsOrbicularRouter(cities, distanceCalculator, new WeightedCenterOrigin()),
				new AutoParamsOrbicularRouter(cities, distanceCalculator, new BoundingBoxCenterOrigin()),
				new AutoParamsOrbicularRouter(cities, distanceCalculator, new FurthestCornerOrigin())
			};
			foreach (var router in routers) {
				result.Add(BuildResults(router, distanceCalculator, 1));
			}
		}
		if (heuristicOrbicular) {
			var routers = new List<AbstractRouter>{
				new HeuristicAutoParamsOrbicularRouter(cities, distanceCalculator, new WeightedCenterOrigin()),
				new HeuristicAutoParamsOrbicularRouter(cities, distanceCalculator, new BoundingBoxCenterOrigin()),
				new HeuristicAutoParamsOrbicularRouter(cities, distanceCalculator, new FurthestCornerOrigin())
			};
			foreach (var router in routers) {
				result.Add(BuildResults(router, distanceCalculator, 1));
			}
		}

		return result;
	}

	private List<Result> BuildResults(AbstractRouter router, ICityDistance distanceCalculator, int times) {
		var results = new List<Result>(times);

		for (int i = 0; i < times; ++i) {
			results.Add(Result.Build(router, distanceCalculator));
		}

		return results;
	}

	private string AnalyzeResults(string performanceAnalysis, float worldWidth, float worldHeight, int numberOfCities,
			bool inCircle, int randomSeed, ICityDistance distanceCalculator, bool bruteForce, bool heldKarp,
			bool nearestNeighbour, bool autoParamsOrbicular, bool heuristicOrbicular, List<List<Result>> newResults) {
		int index = 0;
		string entry = string.Format("\n{0} | {1} | {2} | {3} | {4} | {5}", worldWidth, worldHeight, numberOfCities, inCircle, randomSeed, distanceCalculator.GetType().Name);
		if (bruteForce) {
			var results = newResults[index++];
			entry += AnalyzeResults(results);
		} else {
			entry += AnalyzeResults(null);
		}
		if (heldKarp) {
			var results = newResults[index++];
			entry += AnalyzeResults(results);
		} else {
			entry += AnalyzeResults(null);
		}
		if (nearestNeighbour) {
			var results = newResults[index++];
			entry += AnalyzeResults(results);
		} else {
			entry += AnalyzeResults(null);
		}
		for (int i = 0; i < 3; ++i) {
			var joinChar = i == 0 ? '|' : '/';
			if (autoParamsOrbicular) {
				var results = newResults[index++];
				entry += AnalyzeResults(results, joinChar);
			} else {
				entry += AnalyzeResults(null, joinChar);
			}
		}
		for (int i = 0; i < 3; ++i) {
			var joinChar = i == 0 ? '|' : '/';
			if (heuristicOrbicular) {
				var results = newResults[index++];
				entry += AnalyzeResults(results, joinChar);
			} else {
				entry += AnalyzeResults(null, joinChar);
			}
		}

		return performanceAnalysis + entry;
	}

	private string AnalyzeResults(List<Result> results, char joinChar = '|') {
		if (results == null) {
			return string.Format(" {0} (N/A)", joinChar);
		}

		int count = results.Count;
		Result result;
		if (count == 1) {
			result = results[0];
			return string.Format(" {0} ({1}, {2})", joinChar, result.Distance, result.ExecutionTime);
		}

		result = results[0];
		Vector3 distance = new Vector3(result.Distance, result.Distance, result.Distance);
		Vector3 time = new Vector3(result.ExecutionTime, result.ExecutionTime, result.ExecutionTime);

		for (int i = 1; i < count; ++i) {
			result = results[i];
			if (result.Distance < distance.x) {
				distance.x = result.Distance;
			} else if (result.Distance > distance.z) {
				distance.z = result.Distance;
			}
			distance.y += result.Distance;

			if (result.ExecutionTime < time.x) {
				time.x = result.ExecutionTime;
			} else if (result.ExecutionTime > time.z) {
				time.z = result.ExecutionTime;
			}

			time.y += result.ExecutionTime;
		}

		distance.y /= count;
		time.y /= count;

		return string.Format(" {0} ({1}, {2}) / ({3}, {4}) / ({5}, {6})", joinChar, distance.x, time.x,
				distance.y, time.y, distance.z, time.z);
	}
}
