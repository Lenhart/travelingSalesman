﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(TravelingSalesman))]
public class TravelingSalesmanInspector : Editor {
	private const int MAX_BRUTE_FORCE_CITIES_COUNT = 10;

	private TravelingSalesman travelingSalesman;
	private bool buildCitiesInCircle;

	public override void OnInspectorGUI()
	{
		travelingSalesman = target as TravelingSalesman;

		GUILayout.Label("City generation");
		travelingSalesman.numberOfCities = EditorGUILayout.IntField("Number of cities", travelingSalesman.numberOfCities);
		travelingSalesman.seed = EditorGUILayout.IntField("Seed", travelingSalesman.seed);
		travelingSalesman.minDistance = EditorGUILayout.FloatField("Min city distance", travelingSalesman.minDistance);

		GUILayout.Label("World dimensions");
		travelingSalesman.worldWidth = EditorGUILayout.FloatField("World width", travelingSalesman.worldWidth);
		travelingSalesman.worldHeight = EditorGUILayout.FloatField("World height", travelingSalesman.worldHeight);

		GUILayout.Label("Build");
		buildCitiesInCircle = GUILayout.Toggle(buildCitiesInCircle, "Cities in circle");
		if (GUILayout.Button("Build")) {
			travelingSalesman.seed = (int) System.DateTime.Now.Ticks;
			travelingSalesman.BuildWorld(buildCitiesInCircle, travelingSalesman.seed);
		}
		if (GUILayout.Button("Build from seed")) {
			travelingSalesman.BuildWorld(buildCitiesInCircle, travelingSalesman.seed);
		}
		if (GUILayout.Button("Clear")) {
			travelingSalesman.DestroyWorld();
		}

		if (travelingSalesman.HasWorld()) {
			GUILayout.Label("Routing settings");
			travelingSalesman.distanceCalculatorSetting = (TravelingSalesman.DistanceCalculatorEnum)EditorGUILayout.EnumPopup("Distance calculator", travelingSalesman.distanceCalculatorSetting);
			travelingSalesman.originCalculatorSetting = (TravelingSalesman.OriginCalculatorEnum)EditorGUILayout.EnumPopup("Origin calculator", travelingSalesman.originCalculatorSetting);

			GUILayout.Label("Solvers");
			if (travelingSalesman.BuildCitiesCount() <= MAX_BRUTE_FORCE_CITIES_COUNT && GUILayout.Button("Brute force")) {
				travelingSalesman.Solve(TravelingSalesman.AlgorithmEnum.BruteForce);
			}
			if (GUILayout.Button("Nearest neighbour")) {
				travelingSalesman.Solve(TravelingSalesman.AlgorithmEnum.NearestNeighbour);
			}
			if (GUILayout.Button("Held-Karp")) {
				travelingSalesman.Solve(TravelingSalesman.AlgorithmEnum.HeldKarp);
			}
			// HACK: Too busy to create custom inspector for list
			DrawDefaultInspector();
			if (GUILayout.Button("Orbicular")) {
				travelingSalesman.Solve(TravelingSalesman.AlgorithmEnum.Orbicular);
			}
			if (GUILayout.Button("Auto params Orbicular")) {
				travelingSalesman.Solve(TravelingSalesman.AlgorithmEnum.AutoParamsOrbicular);
			}
			if (GUILayout.Button("Heuristic auto params Orbicular")) {
				travelingSalesman.Solve(TravelingSalesman.AlgorithmEnum.HeuristicAutoParamsOrbicular);
			}
		}

		travelingSalesman.UpdateWorldDimensions();
	}
}
