﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

public class Result
{
	public List<ICity> Cities { get; private set; }
	public int CitiesCount { get { return Cities != null ? Cities.Count : 0; } }
	public float Distance { get; private set; }
	public float ExecutionTime { get; private set; }

	public Result() { }

	public static Result Build(AbstractRouter router, ICityDistance distanceCalculator)
	{
		var stopwatch = new Stopwatch();
		stopwatch.Start();
		var route = router.BuildRoute();
		stopwatch.Stop();

		var result = new Result();
		result.Cities = route;
		result.Distance = AutoParamsOrbicularRouter.CalculateDistance(route, distanceCalculator);
		result.ExecutionTime = stopwatch.ElapsedMilliseconds;
		return result;
	}
}
