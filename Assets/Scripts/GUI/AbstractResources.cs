﻿using System;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public abstract class AbstractCity : MonoBehaviour, ICity, IComparable
{
	public interface ICitySubscriber
	{
		void OnCityPositionSet(AbstractCity city);
	}

	private List<ICitySubscriber> subscribers = new List<ICitySubscriber>();

	public void SetPosition(Vector3 position)
	{
		transform.position = position;
		OnSetPosition();
	}

	public void Subscribe(ICitySubscriber subscriber)
	{
		subscribers.Add(subscriber);
	}

	public void Unsubscribe(ICitySubscriber subscriber)
	{
		subscribers.Remove(subscriber);
	}

	protected virtual void OnSetPosition()
	{
		for (int i = 0; i < subscribers.Count; ++i)
		{
			subscribers[i].OnCityPositionSet(this);
		}
	}
	
	public Vector3 GetPosition()
	{
		return gameObject.transform.position;
	}

	private void Update()
	{
		if (transform.hasChanged)
		{
			OnSetPosition();
			transform.hasChanged = false;
		}
	}

	public int CompareTo(object that) {
		return GetHashCode() - that.GetHashCode();
	}
}

public abstract class AbstractRoad : MonoBehaviour, AbstractCity.ICitySubscriber
{
	protected AbstractCity cityA;
	protected AbstractCity cityB;
	
	public void LayBetween(AbstractCity a, AbstractCity b)
	{
		cityA = a;
		cityB = b;
		a.Subscribe(this);
		b.Subscribe(this);
		UpdateLayout();
	}
	
	protected abstract void UpdateLayout();

	public virtual void OnCityPositionSet(AbstractCity city)
	{
		UpdateLayout();
	}

	public virtual void OnDestroyRoad()
	{
		cityA.Unsubscribe(this);
		cityB.Unsubscribe(this);
	}
}

public abstract class AbstractGround : MonoBehaviour
{
	public abstract void SetSize(float width, float height);
}
