﻿using UnityEngine;

public class SimpleGround : AbstractGround
{
	public override void SetSize(float width, float height)
	{
		transform.localScale = new Vector3(width, 1f, height);
	}
}
