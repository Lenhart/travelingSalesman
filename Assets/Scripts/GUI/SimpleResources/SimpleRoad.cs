﻿using UnityEngine;

public class SimpleRoad : AbstractRoad
{
	protected override void UpdateLayout()
	{
		transform.position = (cityA.GetPosition() + cityB.GetPosition()) / 2f;
		transform.localScale = new Vector3(1f, 1f, (cityA.GetPosition() - cityB.GetPosition()).magnitude);
		transform.rotation = Quaternion.LookRotation(cityB.GetPosition() - cityA.GetPosition());
	}
}
