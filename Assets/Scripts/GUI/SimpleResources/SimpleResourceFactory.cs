﻿using UnityEngine;

public class SimpleResourceFactory : AbstractResourceFactory
{
	private const string CityPath = "Prefabs/SimpleCity";
	private const string RoadPath = "Prefabs/SimpleRoad";
	private const string GroundPath = "Prefabs/SimpleGround";

	private GameObject cityResource;
	private GameObject roadResource;

	public SimpleResourceFactory()
	{
		cityResource = Resources.Load(CityPath) as GameObject;
		roadResource = Resources.Load(RoadPath) as GameObject;
	}

	public override AbstractCity BuildCity(Vector3 position)
	{
		var cityGameObject = GameObject.Instantiate<GameObject>(cityResource);
		var city = cityGameObject.GetComponent<SimpleCity>();
		city.SetPosition(position);
		return city;
	}

	public override AbstractRoad BuildRoad(AbstractCity a, AbstractCity b)
	{
		var roadGameObject = GameObject.Instantiate<GameObject>(roadResource);
		var road = roadGameObject.GetComponent<SimpleRoad>();
		road.LayBetween(a, b);
		return road;
	}

	public override AbstractGround BuildGround()
	{
		var groundGameObject = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>(GroundPath));
		return groundGameObject.GetComponent<AbstractGround>();
	}
}
