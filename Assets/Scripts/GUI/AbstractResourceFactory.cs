﻿using UnityEngine;

public abstract class AbstractResourceFactory
{
	public abstract AbstractCity BuildCity(Vector3 position);
	public abstract AbstractRoad BuildRoad(AbstractCity cityA, AbstractCity cityB);
	public abstract AbstractGround BuildGround();
}
