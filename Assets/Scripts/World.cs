﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class World
{
	private AbstractResourceFactory resourceFactory;
	private List<AbstractCity> cities { get; set; }
	private List<AbstractRoad> roads;
	private AbstractGround ground;

	private List<ICity> cachedICityList;
	private List<AbstractCity> cachedForCities;
	public List<ICity> Cities
	{
		get
		{
			if (cachedICityList == null || cachedForCities != cities || cachedICityList.Count != cities.Count)
			{
				cachedForCities = cities;
				cachedICityList = cities.ConvertAll((ac) => (ICity)ac);
			}
			return cachedICityList;
		}
	}
	public int CitiesCount { get { return Cities != null ? Cities.Count : 0; }}

	public World()
	{
		resourceFactory = new SimpleResourceFactory();
		cities = new List<AbstractCity>();
		roads = new List<AbstractRoad>();
		ground = resourceFactory.BuildGround();
	}

	public void BuildCity(Vector3 position)
	{
		var city = resourceFactory.BuildCity(position);
		cities.Add(city);
	}

	public void BuildRoads(Result result)
	{
		DestroyRoads();
		for (int i = 0; i < result.CitiesCount - 1; ++i)
		{
			var road = resourceFactory.BuildRoad(result.Cities[i] as AbstractCity, result.Cities[i + 1] as AbstractCity);
			roads.Add(road);
		}
		if (result.CitiesCount > 2)
		{
			var road = resourceFactory.BuildRoad(result.Cities[result.CitiesCount - 1] as AbstractCity, result.Cities[0] as AbstractCity);
			roads.Add(road);
		}
	}

	public void Destroy()
	{
		try
		{
			DestroyRoads();
			DestroyCities();
			GameObject.DestroyImmediate(ground.gameObject);
		} catch (Exception)
		{
			// Do nothing, probably the cities were cleaned up manually
		}
	}

	private void DestroyCities()
	{
		for (int i = 0; i < cities.Count; ++i)
		{
			GameObject.DestroyImmediate(cities[i].gameObject);
		}
		cities = new List<AbstractCity>(); // to be sure that cached List<ICity> will be updated accordingly
	}

	private void DestroyRoads()
	{
		for (int i = 0; i < roads.Count; ++i)
		{
			roads[i].OnDestroyRoad();
			GameObject.DestroyImmediate(roads[i].gameObject);
		}
		roads.Clear();
	}

	public void SetWorldDimensions(float width, float height)
	{
		ground.SetSize(width, height);
	}
}
