﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PerformanceAnalyzer : MonoBehaviour {
	public class City : ICity, IComparable {
		private readonly Vector3 position;

		public City(Vector3 position) {
			this.position = position;
		}

		public Vector3 GetPosition() {
			return position;
		}

		public int CompareTo(object that) {
			return this.GetHashCode() - that.GetHashCode();
		}
	}

	public const int NEAREST_NEIGHBOUR_TIMES = 10;

	// Set to 0 for unlimited
	// Set to negative to disable algorithm
	public int AnalyzeBruteForce = 5;
	public int AnalyzeHeldKarp = 10;
	public int AnalyzeNearestNeighbour;
	public int AnalyzeAutoParamsOrbicular = 80;
	public int AnalyzeHeuristicOrbicular;

	public List<Vector2> worldDimensions = new List<Vector2>{ new Vector2(100f, 100f) };
	public List<int> numberOfCities = new List<int>{ 10 };
	public List<bool> citiesInCircle = new List<bool>{ true };

	public List<TravelingSalesman.DistanceCalculatorEnum> distanceCalculators = new List<TravelingSalesman.DistanceCalculatorEnum>{
		TravelingSalesman.DistanceCalculatorEnum.EuclidDistance
	};

	public int startingRandomSeed = 1;
}
