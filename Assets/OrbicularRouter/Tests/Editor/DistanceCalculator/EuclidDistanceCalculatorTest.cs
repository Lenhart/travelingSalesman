﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using NSubstitute;

public class EuclidDistanceCalculatorTest
{
	public static List<TestCaseData> DistanceTestCases()
	{
		var tests = new List<TestCaseData>();
		
		var c0 = Vector3.zero;
		var c1 = new Vector3(1f, 0f, 0f);
		var c2 = new Vector3(0f, 0f, 2f);
		
		tests.Add(new TestCaseData(c0, c0).Returns(0f));
		tests.Add(new TestCaseData(c0, c1).Returns(1f));
		tests.Add(new TestCaseData(c2, c0).Returns(2f));
		tests.Add(new TestCaseData(c1, c2).Returns(Mathf.Sqrt(5f)));
		
		return tests;
	}
	
	[Test, TestCaseSource("DistanceTestCases")]
	public float TestDistance(Vector3 from, Vector3 to)
	{
		var calculator = EuclidDistanceCalculator.GetInstance();
		return calculator.GetDistance(from, to);
	}
}
