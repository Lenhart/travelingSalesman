﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using NSubstitute;

public class MaxDistanceCalculatorTest
{
	public static List<TestCaseData> DistanceTestCases()
	{
		var tests = new List<TestCaseData>();
		
		ICity c0 = Substitute.For<ICity>();
		c0.GetPosition().Returns(Vector3.zero);
		
		ICity c1 = Substitute.For<ICity>();
		c1.GetPosition().Returns(new Vector3(1f, 0f, 0f));
		
		ICity c2 = Substitute.For<ICity>();
		c2.GetPosition().Returns(new Vector3(0f, 0f, 2f));
		
		tests.Add(new TestCaseData(c0, c0).Returns(0f));
		tests.Add(new TestCaseData(c0, c1).Returns(1f));
		tests.Add(new TestCaseData(c2, c0).Returns(2f));
		tests.Add(new TestCaseData(c1, c2).Returns(2f));
		
		return tests;
	}
	
	[Test, TestCaseSource("DistanceTestCases")]
	public float TestDistance(ICity from, ICity to)
	{
		var calculator = MaxDistanceCalculator.GetInstance();
		return calculator.GetDistance(from.GetPosition(), to.GetPosition());
	}
}
