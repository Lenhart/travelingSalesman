using System;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;

public class AbstractRouterTest
{
	private class TestRouter : AbstractRouter
	{
		public enum BuildResultEnum
		{
			Null,
			Unique,
			Intersect,
			OtherCities
		}

		public int RouteCallCount { get; private set; }
		private BuildResultEnum BuildResult { get; set; }

		public TestRouter(List<ICity> cities, ICityDistance distanceCalculator, BuildResultEnum buildResult = BuildResultEnum.Unique)
			: base(cities, distanceCalculator)
		{
			RouteCallCount = 0;
			BuildResult = buildResult;
		}

		protected override List<ICity> Route()
		{
			RouteCallCount++;
			List<ICity> cities;
			switch (BuildResult)
			{
			case BuildResultEnum.Null:
				cities = null;
				break;
			case BuildResultEnum.Unique:
				cities = Cities;
				break;
			case BuildResultEnum.Intersect:
				cities = new List<ICity>(Cities);
				cities.RemoveAt(0);
				var matchCity = cities[0];
				cities.Add(matchCity);
				break;
			case BuildResultEnum.OtherCities:
				cities = new List<ICity>();
				for (int i = 0; i < CitiesCount; i++)
				{
					var city = Substitute.For<ICity>();
					cities.Add(city);
				}
				break;
			default:
				throw new NotImplementedException();
			}
			return cities;
		}
	}

	protected List<ICity> MockCities(int count)
	{
		var cities = new List<ICity>();
		for (int i = 0; i < count; i++)
		{
			var city = Substitute.For<ICity>();
			cities.Add(city);
		}
		return cities;
	}

	[Test]
	public void TestConstructorThrowsExceptionForEmptyCities()
	{
		Assert.That(() => new TestRouter(null, EuclidDistanceCalculator.GetInstance()), Throws.ArgumentException);

		var empty = MockCities(0);
		Assert.That(() => new TestRouter(empty, EuclidDistanceCalculator.GetInstance()), Throws.ArgumentException);
	}

	[Test]
	public void TestConstructorThrowsExceptionForEmptyDistanceCalculator()
	{
		var cities = MockCities(2);
		Assert.That(() => new TestRouter(cities, null), Throws.TypeOf<ArgumentNullException>());
	}

	[Test]
	public void TestRoutingIsDoneEveryTime()
	{
		var cities = MockCities(3);
		var router = new TestRouter(cities, EuclidDistanceCalculator.GetInstance());

#pragma warning disable 0168
		Assert.That(router.RouteCallCount, Is.EqualTo(0));
		var result = router.BuildRoute();
		Assert.That(router.RouteCallCount, Is.EqualTo(1));

		result = router.BuildRoute();
		Assert.That(router.RouteCallCount, Is.EqualTo(2));
#pragma warning restore 0168
	}

	[Test]
	public void TestMissingCityThrowsException()
	{
		var cities = MockCities(3);
		var router = new TestRouter(cities, EuclidDistanceCalculator.GetInstance(), TestRouter.BuildResultEnum.OtherCities);
#pragma warning disable 0168
		Assert.That(() => { var r = router.BuildRoute(); }, Throws.InvalidOperationException);

		router = new TestRouter(cities, EuclidDistanceCalculator.GetInstance(), TestRouter.BuildResultEnum.Null);
		Assert.That(() => { var r = router.BuildRoute(); }, Throws.InvalidOperationException);
#pragma warning restore 0168
	}

	[Test]
	public void TestAddedCityThrowsException()
	{
		var cities = MockCities(3);
		var router = new TestRouter(cities, EuclidDistanceCalculator.GetInstance(), TestRouter.BuildResultEnum.Intersect);
#pragma warning disable 0168
		Assert.That(() => { var r = router.BuildRoute(); }, Throws.InvalidOperationException);
#pragma warning restore 0168
	}
}
