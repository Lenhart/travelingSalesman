﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class OrbicularRouterTest : CommonTest
{
	private List<float> constructorRadiuses;

	protected override AbstractRouter BuildRouter(List<ICity> cities, ICityDistance distanceCalculator) {
		return new OrbicularRouter(cities, distanceCalculator, new WeightedCenterOrigin(), constructorRadiuses);
	}

	public static List<TestCaseData> HardcodedValuesTestSource()
	{
		var tests = new List<TestCaseData>();

		var maxExpectedTime = 20f;	// algorithm is fast, should be less than 5ms, but use larger value here so we don't get false negatives often
		tests.Add(new TestCaseData(null, maxExpectedTime, 73f));
		tests.Add(new TestCaseData(new List<float>{ 2f, 7f }, maxExpectedTime, 81f));

		return tests;
	}

	/// <summary>
	/// Tests the hardcoded values.
	/// Note, if tst fails on execution time, just give it another run. It may be a random glitch.
	/// </summary>
	[Test, TestCaseSource("HardcodedValuesTestSource")]
	public void TestHardcodedValues(List<float> radiuses, float maxExpectedTime, float expectedDistance)
	{
		var positions = new List<Vector3>{
			V3(5f, 0f),
			V3(5f, 1f),
			V3(5f, 2f),
			V3(2f, 4f),
			V3(1f, -4f),
			V3(5f, -2f),
			V3(5f, -1f),
			V3(0f, 10f),
			V3(0f, -10f),
			V3(-2f, -10f),
			V3(-2f, 10f)
		};
		var cities = MockCities(positions);
		constructorRadiuses = radiuses;
		var router = BuildRouter(cities, EuclidDistanceCalculator.GetInstance());
		var result = TestResult.Build(router, EuclidDistanceCalculator.GetInstance());
		
		Assert.That(result.ExecutionTime, Is.LessThan(maxExpectedTime));
		Assert.That(result.Distance, Is.EqualTo(expectedDistance).Within(1f));
	}
}
