﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class NearestNeighbourRouterTest : CommonTest
{
	protected override AbstractRouter BuildRouter(List<ICity> cities, ICityDistance distanceCalculator) {
		return new NearestNeighbourRouter(cities, EuclidDistanceCalculator.GetInstance());
	}

	[Test]
	public void TestHardcodedValues()
	{
		var positions = new List<Vector3>{
			V3(5f, 0f),
			V3(5f, 1f),
			V3(5f, 2f),
			V3(2f, 4f),
			V3(1f, -4f),
			V3(5f, -2f),
			V3(5f, -1f),
			V3(0f, 10f),
			V3(0f, -10f),
			V3(-2f, -10f),
			V3(-2f, 10f)
		};
		var cities = MockCities(positions);
		var router = BuildRouter(cities, EuclidDistanceCalculator.GetInstance());
		var result = TestResult.Build(router, EuclidDistanceCalculator.GetInstance());

		Assert.That(result.ExecutionTime, Is.LessThan(15f));	// algorithm is relatively fast, should be less than 10ms, but use larger value here so we don't get false negatives often
		Assert.That(result.Distance, Is.InRange(45f, 55f));		// depends on the run, so check on range is mandatory
	}
}
