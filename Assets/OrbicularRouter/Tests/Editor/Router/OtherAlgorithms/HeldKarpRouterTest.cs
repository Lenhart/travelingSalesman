﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class HeldKarpRouterTest : CommonTest
{
	protected override AbstractRouter BuildRouter(List<ICity> cities, ICityDistance distanceCalculator) {
		return new HeldKarpRouter(cities, distanceCalculator);
	}

	[Test]
	public void TestHardcodedValues()
	{
		var positions = new List<Vector3>{
			V3(5f, 0f),
			V3(2f, 4f),
			V3(1f, -4f),
			V3(5f, -2f),
			V3(5f, -1f),
			V3(0f, 10f),
			V3(0f, -10f),
			V3(-2f, -10f),
			V3(-2f, 10f)
		};
		var cities = MockCities(positions);
		var router = BuildRouter(cities, EuclidDistanceCalculator.GetInstance());
		var result = TestResult.Build(router, EuclidDistanceCalculator.GetInstance());
		
		Assert.That(result.ExecutionTime, Is.LessThan(500f));
		Assert.That(result.Distance, Is.EqualTo(47.78f).Within(0.01f));
	}
}
