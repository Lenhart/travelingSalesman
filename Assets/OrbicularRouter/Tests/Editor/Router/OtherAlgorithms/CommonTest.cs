﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using NSubstitute;

public abstract class CommonTest
{
	protected abstract AbstractRouter BuildRouter(List<ICity> cities, ICityDistance distanceCalculator);
	
	protected List<ICity> MockCities(int citiesCount)
	{
		var positions = new List<Vector3>();
		for (int i = 0; i < citiesCount; i++)
		{
			positions.Add(new Vector3(i, 0, i));
		}
		return MockCities(positions);
	}
	
	protected List<ICity> MockCities(List<Vector3> cityPositions)
	{
		var cities = new List<ICity>();
		for (int i = 0; i < cityPositions.Count; i++)
		{
			var city = BuildCity(cityPositions[i]);
			cities.Add(city);
		}
		return cities;
	}
	
	protected virtual ICity BuildCity(Vector3 position)
	{
		var city = Substitute.For<ICity>();
		city.GetPosition().Returns(position);
		return city;
	}
	
	protected Vector3 V3(float x, float z)
	{
		return new Vector3(x, z);
	}
	
	[Test]
	public void TestNullCities()
	{
		Assert.That(() => BuildRouter(null, EuclidDistanceCalculator.GetInstance()), Throws.ArgumentException);
	}
	
	[Test]
	public void TestEmptyCities()
	{
		var emptyList = new List<ICity>();
		Assert.That(() => BuildRouter(emptyList, EuclidDistanceCalculator.GetInstance()), Throws.ArgumentException);
	}
	
	[Test]
	public void TestCitiesCount(
		[Values(1, 2, 10)] int citiesCount
	)
	{
		var cities = MockCities(citiesCount);
		var router = BuildRouter(cities, EuclidDistanceCalculator.GetInstance());
		
		var resultCities = router.BuildRoute();
		Assert.That(resultCities.Count, Is.EqualTo(citiesCount));
	}
	
	[Test]
	public void TestCitiesUnique(
		[Values (2, 10)] int citiesCount
		)
	{
		var cities = MockCities(citiesCount);
		var router = BuildRouter(cities, EuclidDistanceCalculator.GetInstance());
		var resultCities = router.BuildRoute();
		
		var visited = new HashSet<ICity>();
		foreach (var city in resultCities)
		{
			Assert.That(visited.Add(city), Is.True);
		}
	}
}
