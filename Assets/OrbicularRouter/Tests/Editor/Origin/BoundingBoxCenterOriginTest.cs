﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using NSubstitute;

public class BoundingBoxCenterOriginTest
{
	private static ICity C(float x, float y, float z)
	{
		var city = Substitute.For<ICity>();
		city.GetPosition().Returns(new Vector3(x, y, z));
		return city;
	}

	public static List<TestCaseData> TestCases()
	{
		var tests = new List<TestCaseData>();

		tests.Add(new TestCaseData(null).Returns(Vector3.zero));
		tests.Add(new TestCaseData(new List<ICity>()).Returns(Vector3.zero));
		tests.Add(new TestCaseData(new List<ICity>{
			C(0f, 0f, 2f),
			C(0f, -3f, 1f),
			C(2f, 2f, 2f)
		}).Returns(new Vector3(1f, -0.5f, 1.5f)));
		tests.Add(new TestCaseData(new List<ICity>{
			C(1f, 1f, 2f),
			C(1f, 1f, -1f),
			C(1f, -2f, 1f),
			C(1f, -1f, -1f),
			C(-4f, 1f, 7f),
			C(-1f, 1f, -1f),
			C(-1f, -1f, 1f),
			C(-1f, -1f, -1f),
		}).Returns(new Vector3(-1.5f, -0.5f, 3f)));
		
		return tests;
	}
	
	[Test, TestCaseSource("TestCases")]
	public Vector3 TestGetOrigin(List<ICity> cities)
	{
		var calculator = new BoundingBoxCenterOrigin();
		return calculator.GetOrigin(cities);
	}
}
