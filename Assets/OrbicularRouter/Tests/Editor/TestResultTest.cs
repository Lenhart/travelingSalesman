﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NSubstitute;
using NUnit.Framework;

public class TestResultTest
{
	private class TestRouter : AbstractRouter
	{
		public TestRouter(List<ICity> cities)
			: base(cities, EuclidDistanceCalculator.GetInstance())
		{
		}

		protected override List<ICity> Route()
		{
			return new List<ICity>(Cities);
		}
	}

	private class TestDistanceCalculator : ICityDistance
	{
		public const float Multiplier = 2f;

		public int TimesCalled { get; private set; }
		private HashSet<Vector3> fromSet = new HashSet<Vector3>();
		private HashSet<Vector3> toSet = new HashSet<Vector3>();

		public TestDistanceCalculator()
		{
			TimesCalled = 0;
		}

		public float GetDistance(Vector3 from, Vector3 to)
		{
			if (!fromSet.Add(from))
			{
				throw new Exception("Same city is twice used in FROM");
			}
			if (!toSet.Add(from))
			{
				throw new Exception("Same city is twice used in TO");
			}
			TimesCalled++;
			return Multiplier;
		}
	}

	private List<ICity> BuildCities(int count)
	{
		var cities = new List<ICity>();
		for (int i = 0; i < count; i++)
		{
			var city = Substitute.For<ICity>();
			city.GetPosition().Returns(new Vector3(i, i, i));
			cities.Add(city);
		}
		return cities;
	}

	[Test]
	public void TestCitiesCount(
		[Values (1, 5)] int expectedCount
	)
	{
		var cities = BuildCities(expectedCount);
		var router = new TestRouter(cities);
		var result = TestResult.Build(router, EuclidDistanceCalculator.GetInstance());

		Assert.That(result.CitiesCount, Is.EqualTo(expectedCount));
		Assert.That(result.Cities.Count, Is.EqualTo(expectedCount));
	}

	[Test]
	public void TestDistance(
		[Values (1, 2, 5)] int citiesCount
	)
	{
		var cities = BuildCities(citiesCount);
		var router = new TestRouter(cities);

		var distanceCalculator = new TestDistanceCalculator();
		var result = TestResult.Build(router, distanceCalculator);

		float expectedDistance;
		int expectedTimesCalled;
		if (citiesCount == 1)
		{
			expectedDistance = 0;
			expectedTimesCalled = 0;
		}
		else
		{
			expectedDistance = citiesCount * TestDistanceCalculator.Multiplier;
			expectedTimesCalled = citiesCount;
		}
		Assert.That(result.Distance, Is.EqualTo(expectedDistance));
		Assert.That(distanceCalculator.TimesCalled, Is.EqualTo(expectedTimesCalled));
	}
}
