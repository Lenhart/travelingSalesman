﻿using System.Collections.Generic;
using NUnit.Framework;

public class BitContextMapTest
{
	public List<TestCaseData> CalculateBitsSetTestCases()
	{
		var list = new List<TestCaseData>();

		list.Add(new TestCaseData(1).Returns(1));
		list.Add(new TestCaseData(2).Returns(1));
		list.Add(new TestCaseData(5).Returns(2));
		list.Add(new TestCaseData(10).Returns(2));
		list.Add(new TestCaseData(15).Returns(4));
		list.Add(new TestCaseData(255).Returns(8));
		list.Add(new TestCaseData(256).Returns(1));
		list.Add(new TestCaseData(0x12345678).Returns(13));

		return list;
	}

	[Test, TestCaseSource("CalculateBitsSetTestCases")]
	public int TestCalculateBitsSet(long value)
	{
		return BitContextMap.CalculateBitsSet(value);
	}

	[Test]
	public void TestCtorThrowsExceptionForInvalidResolution([Values (-1, 0)] int resolution)
	{
		Assert.That(() => new BitContextMap(resolution), Throws.ArgumentException);
	}

	[Test]
	public void TestCtorThrowsExceptionForInvalidMaxPosition([Values (0f, -1f)] float maxPosition)
	{
		Assert.That(() => new BitContextMap(32, maxPosition), Throws.ArgumentException);
	}

	public List<TestCaseData> TestCases()
	{
		var list = new List<TestCaseData>();

		// empty map
		list.Add(new TestCaseData(32, new List<float>{ }, 0, 0f));
		// out of range gets clamped
		list.Add(new TestCaseData(32, new List<float>{ -1f }, 1, 1f / 32));
		list.Add(new TestCaseData(32, new List<float>{ 361f }, 1, 1f / 32));
		// single value
		list.Add(new TestCaseData(32, new List<float>{ 12f }, 1, 1f / 32));
		list.Add(new TestCaseData(32, new List<float>{ 360f }, 1, 1f / 32));
		// set same bit twice
		list.Add(new TestCaseData(4, new List<float>{ 20f, 25f }, 1, 0.25f));
		// multiple bits
		list.Add(new TestCaseData(4, new List<float>{ 12f, 180f }, 2, 0.5f));
		// full map
		list.Add(new TestCaseData(4, new List<float>{ 10f, 100f, 190f, 280f }, 4, 1f));

		return list;
	}

	[Test, TestCaseSource("TestCases")]
	public void TestBitsSet(int resolution, List<float> positions, int bitsSet, float unused)
	{
		var map = InitializeMap(resolution, positions);
		Assert.That(map.GetBitsSet(), Is.EqualTo(bitsSet));
	}

	[Test, TestCaseSource("TestCases")]
	public void TestFillPercentage(int resolution, List<float> positions, int unused, float fillPercentage)
	{
		var map = InitializeMap(resolution, positions);
		Assert.That(map.GetFillPercentage(), Is.EqualTo(fillPercentage));
	}

	[Test]
	public void TestClear()
	{
		var map = InitializeMap(4, new List<float>{ 123f });
		map.Clear();

		Assert.That(map.GetBitsSet(), Is.EqualTo(0));
		Assert.That(map.GetFillPercentage(), Is.EqualTo(0f));
	}

	private BitContextMap InitializeMap(int resolution, List<float> positions)
	{
		var map = new BitContextMap(resolution);
		for (int i = 0; i < positions.Count; i++)
		{
			map.Occupy(positions[i]);
		}
		return map;
	}
}
