﻿using System.Collections.Generic;
using NUnit.Framework;

public class Float01RadixSortTest
{
	public List<TestCaseData> TestCases()
	{
		var tests = new List<TestCaseData>();

		// empty list
		tests.Add(new TestCaseData(null).Returns(null));
		// single element
		tests.Add(new TestCaseData(new List<int> { 0 }).Returns(new List<int>{ 0 }));
		tests.Add(new TestCaseData(new List<int> { 99 }).Returns(new List<int>{ 99 }));
		// two elements
		tests.Add(new TestCaseData(new List<int> { 1, 2 }).Returns(new List<int>{ 1, 2 }));
		tests.Add(new TestCaseData(new List<int> { 2, 1 }).Returns(new List<int>{ 1, 2 }));
		// list with arbitrary number of elements
		tests.Add(new TestCaseData(new List<int> { 12, 0, 4, 5, 23, 99 }).Returns(new List<int>{ 0, 4, 5, 12, 23, 99 }));
		// sorted list
		tests.Add(new TestCaseData(new List<int> { 0, 3, 7, 23, 45, 57 }).Returns(new List<int>{ 0, 3, 7, 23, 45, 57 }));
		// same digit multiple times on the same place
		tests.Add(new TestCaseData(new List<int> { 20, 21, 2, 28, 26 }).Returns(new List<int>{ 2, 20, 21, 26, 28 }));

		return tests;
	}

	[Test, TestCaseSource("TestCases")]
	public List<int> Test(List<int> list)
	{
		Float01RadixSort<int>.Sort(list, (i) => i / 100f);
		return list;
	}

	[Test]
	public void TestValueUnderRange()
	{
		var list = new List<float>{ 0.1f, 0.2f, -0.00001f, 0.5f };
		Assert.That(() => Float01RadixSort<float>.Sort(list, (f) => f), Throws.ArgumentException);
	}

	[Test]
	public void TestValueOverRange()
	{
		var list = new List<float>{ 0.1f, 0.2f, 1f, 0.5f };
		Assert.That(() => Float01RadixSort<float>.Sort(list, (f) => f), Throws.ArgumentException);
	}
}
