﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class ContextMapTest
{
	[Test]
	public void TestCtorThrowsExceptionForInvalidResolution([Values (-1, 0, 1)] int resolution) {
		Assert.That(() => new ContextMap(resolution), Throws.ArgumentException);
	}

	[Test]
	public void TestCtorThrowsExceptionForInvalidMaxPosition([Values (-1f, 0f)] float maxPosition) {
		Assert.That(() => new ContextMap(16, maxPosition), Throws.ArgumentException);
	}

	[Test]
	public void TestReset() {
		var map = new ContextMap();
		map.Occupy(20f, 1f, 30f);
		map.Reset();
		var breach = map.FindBreach();
		Assert.That(breach, Is.EqualTo(new Vector3(180f, 360f, 0f)));
	}

	public List<TestCaseData> TestCases() {
		List<TestCaseData> testCases = new List<TestCaseData>();

		// empty map
		testCases.Add(new TestCaseData(16, 360f, new List<Vector3>(), 180f, 360f));
		// single entry in middle of map, breach at start
		testCases.Add(new TestCaseData(36, 360f, new List<Vector3>{
			new Vector3(180f, 1f, 299f)
		}, 0f, 60f));
		// single entry at start of map, breach in middle
		testCases.Add(new TestCaseData(36, 360f, new List<Vector3>{
			new Vector3(0f, 1f, 299f)
		}, 180f, 60f));
		// multiple writes, overlapping
		testCases.Add(new TestCaseData(36, 360f, new List<Vector3>{
			new Vector3(0f, 1f, 299f),
			new Vector3(20f, 1f, 80f)
		}, 180f, 60f));
		// multiple writes covering whole map, should behave the same as empty map
		testCases.Add(new TestCaseData(36, 360f, new List<Vector3>{
			new Vector3(5f, 1f, 88f),
			new Vector3(95f, 1f, 88f),
			new Vector3(185f, 1f, 88f),
			new Vector3(275f, 1f, 88f)
		}, 180f, 360f));
		// multiple writes, last one with bigger intensity
		testCases.Add(new TestCaseData(36, 360f, new List<Vector3>{
			new Vector3(5f, 1f, 88f),
			new Vector3(95f, 1f, 88f),
			new Vector3(185f, 1f, 88f),
			new Vector3(275f, 2f, 88f)
		}, 90f, 270f));
		// multiple writes, last one with bigger intensity by writing twice
		testCases.Add(new TestCaseData(36, 360f, new List<Vector3>{
			new Vector3(5f, 1f, 88f),
			new Vector3(95f, 1f, 88f),
			new Vector3(185f, 1f, 88f),
			new Vector3(275f, 1f, 88f),
			new Vector3(275f, 1f, 88f)
		}, 90f, 270f));

		return testCases;
	}

	[Test, TestCaseSource("TestCases")]
	public void TestBreachPosition(int resolution, float maxPosition, List<Vector3> occupyList, float expectedPosition, float unused) {
		var map = BuildMap(resolution, maxPosition, occupyList);
		var breach = map.FindBreach();
		Assert.That(breach.x, Is.EqualTo(expectedPosition));
	}

	[Test, TestCaseSource("TestCases")]
	public void TestBreachWidth(int resolution, float maxPosition, List<Vector3> occupyList, float unused, float expectedWidth) {
		var map = BuildMap(resolution, maxPosition, occupyList);
		var breach = map.FindBreach();
		Assert.That(breach.y, Is.EqualTo(expectedWidth));
	}

	private ContextMap BuildMap(int resolution, float maxPosition, List<Vector3> occupyList) {
		var map = new ContextMap(resolution, maxPosition);
		for (int i = 0; i < occupyList.Count; ++i) {
			var occupy = occupyList[i];
			map.Occupy(occupy.x, occupy.y, occupy.z);
		}
		return map;
	}
}
