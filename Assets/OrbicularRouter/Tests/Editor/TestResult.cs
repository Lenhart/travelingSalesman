﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

public class TestResult
{
	public List<ICity> Cities { get; private set; }
	public int CitiesCount { get { return Cities != null ? Cities.Count : 0; } }
	public float Distance { get; private set; }
	public float ExecutionTime { get; private set; }

	public TestResult() { }

	public static TestResult Build(AbstractRouter router, ICityDistance distanceCalculator)
	{
		var stopwatch = new Stopwatch();
		stopwatch.Start();
		var route = router.BuildRoute();
		stopwatch.Stop();

		var distance = 0f;
		var citiesCount = route.Count;
		for (int i = 0; i < citiesCount - 1; ++i) {
			distance += distanceCalculator.GetDistance(route[i].GetPosition(), route[i + 1].GetPosition());
		}
		if (citiesCount > 1) {
			distance += distanceCalculator.GetDistance(route[citiesCount - 1].GetPosition(), route[0].GetPosition());
		}

		var result = new TestResult();
		result.Cities = route;
		result.Distance = distance;
		result.ExecutionTime = stopwatch.ElapsedMilliseconds;
		return result;
	}
}
