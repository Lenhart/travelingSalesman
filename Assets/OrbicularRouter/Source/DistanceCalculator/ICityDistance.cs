﻿using UnityEngine;

public interface ICityDistance
{
	float GetDistance(Vector3 from, Vector3 to);
}