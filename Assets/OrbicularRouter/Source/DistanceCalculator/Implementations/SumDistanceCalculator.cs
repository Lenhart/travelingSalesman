﻿using System;
using UnityEngine;

public class SumDistanceCalculator : ICityDistance
{
	public float GetDistance(Vector3 from, Vector3 to)
	{
		var xDiff = Math.Abs(from.x - to.x);
		var yDiff = Math.Abs(from.y - to.y);
		var zDiff = Math.Abs(from.z - to.z);

		return xDiff + yDiff + zDiff;
	}

	public static ICityDistance GetInstance()
	{
		return instance ?? (instance = new SumDistanceCalculator());
	}

	private SumDistanceCalculator() { }

	private static ICityDistance instance;
}
