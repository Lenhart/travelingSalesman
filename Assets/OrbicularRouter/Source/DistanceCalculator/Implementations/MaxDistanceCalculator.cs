﻿using System;
using UnityEngine;

public class MaxDistanceCalculator : ICityDistance
{
	public float GetDistance(Vector3 from, Vector3 to)
	{
		var xDiff = Math.Abs(from.x - to.x);
		var yDiff = Math.Abs(from.y - to.y);
		var zDiff = Math.Abs(from.z - to.z);

		var xyMax = Math.Max(xDiff, yDiff);
		return Math.Max(xyMax, zDiff);
	}

	public static ICityDistance GetInstance()
	{
		return instance ?? (instance = new MaxDistanceCalculator());
	}

	private MaxDistanceCalculator() { }

	private static ICityDistance instance;
}
