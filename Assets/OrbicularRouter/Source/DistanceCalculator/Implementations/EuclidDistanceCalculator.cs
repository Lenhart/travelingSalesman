﻿using UnityEngine;

public class EuclidDistanceCalculator : ICityDistance
{
	public float GetDistance(Vector3 from, Vector3 to)
	{
		return (from - to).magnitude;
	}

	public static ICityDistance GetInstance()
	{
		return instance ?? (instance = new EuclidDistanceCalculator());
	}

	private EuclidDistanceCalculator() { }

	private static ICityDistance instance;
}
