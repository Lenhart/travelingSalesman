﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoundingBoxCenterOrigin : AbstractQuadrantedOrigin
{
	public override float FullAngleFactor()
	{
		return 1f;
	}

	protected override Vector3 FilterQuadrant(Vector3?[] quadrants)
	{
		float xMin = 0;
		float xMax = 0;
		float yMin = 0;
		float yMax = 0;
		float zMin = 0;
		float zMax = 0;

		bool initialized = false;

		for (int i = 0; i < quadrants.Length; i++)
		{
			var vectorOrNull = quadrants[i];
			if (!vectorOrNull.HasValue) continue;
			var vector = vectorOrNull.Value;

			if (initialized)
			{
				if (vector.x > 0 && vector.x > xMax) { xMax = vector.x; }
				else if (vector.x < 0 && vector.x < xMin) { xMin = vector.x; }
				if (vector.y > 0 && vector.y > yMax) { yMax = vector.y; }
				else if (vector.y < 0 && vector.y < yMin) { yMin = vector.y; }
				if (vector.z > 0 && vector.z > zMax) { zMax = vector.z; }
				else if (vector.z < 0 && vector.z < zMin) { zMin = vector.z; }
			}
			else
			{
				initialized = true;
				xMin = vector.x;
				xMax = vector.x;
				yMin = vector.y;
				yMax = vector.y;
				zMin = vector.z;
				zMax = vector.z;
			}
		}
		float x = (xMin + xMax) / 2f;
		float y = (yMin + yMax) / 2f;
		float z = (zMin + zMax) / 2f;
		return new Vector3(x, y, z);
	}
}
