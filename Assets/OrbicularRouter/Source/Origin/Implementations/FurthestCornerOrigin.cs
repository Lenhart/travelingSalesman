﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FurthestCornerOrigin : AbstractQuadrantedOrigin
{
	public override float FullAngleFactor()
	{
		return 0.25f;
	}

	protected override Vector3 FilterQuadrant(Vector3?[] quadrants)
	{
		return quadrants.Aggregate((curMax, vector) => (vector.GetValueOrDefault(Vector3.zero).sqrMagnitude > curMax.GetValueOrDefault(Vector3.zero).sqrMagnitude) ? vector : curMax).GetValueOrDefault(Vector3.zero);
	}
}
