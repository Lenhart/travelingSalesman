﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractQuadrantedOrigin : AbstractCachedOrigin
{
	protected abstract Vector3 FilterQuadrant(Vector3?[] quadrants);

	protected override Vector3 CalculateOrigin(List<ICity> cities)
	{
		if (cities.Count <= 0) return Vector3.zero;
		
		Vector3?[] quadrants = new Vector3?[8];
		for (int i = 0; i < 8; i++)
		{
			quadrants[i] = new Vector3?();
		}
		
		for (int i = 0; i < cities.Count; i++)
		{
			var position = cities[i].GetPosition();
			int index = GetQuadrantIndex(position);
			if (position.sqrMagnitude > quadrants[index].GetValueOrDefault(Vector3.zero).sqrMagnitude)
			{
				quadrants[index] = position;
			}
		}

		return FilterQuadrant(quadrants);
	}
	
	private int GetQuadrantIndex(Vector3 vector)
	{
		bool condition = vector.x > 0;
		int index = Convert.ToInt32(condition);
		
		condition = vector.y > 0;
		index = index << 1 | Convert.ToInt32(condition);
		
		condition = vector.z > 0;
		return index << 1 | Convert.ToInt32(condition);
	}
}
