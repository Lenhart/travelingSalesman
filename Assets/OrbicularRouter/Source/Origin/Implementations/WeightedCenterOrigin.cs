﻿using System.Collections.Generic;
using UnityEngine;

public class WeightedCenterOrigin : AbstractCachedOrigin
{
	protected override Vector3 CalculateOrigin(List<ICity> cities)
	{
		Vector3 result = Vector3.zero;
		if (cities.Count > 0)
		{
			for (int i = 0; i < cities.Count; i++)
			{
				result = result + cities[i].GetPosition();
			}
			result = result / cities.Count;
		}
		return result;
	}

	public override float FullAngleFactor()
	{
		return 1f;
	}
}
