﻿using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractCachedOrigin : IOrigin
{
	private List<ICity> cachedCities;
	private Vector3 cachedResult;

	public Vector3 GetOrigin(List<ICity> cities)
	{
		if (cachedCities != cities)
		{
			cachedCities = cities;
			cachedResult = CalculateOrigin(cities);
		}
		return cachedResult;
	}

	protected abstract Vector3 CalculateOrigin(List<ICity> cities);
	public abstract float FullAngleFactor();
}
