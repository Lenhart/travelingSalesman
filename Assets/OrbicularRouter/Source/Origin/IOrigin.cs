﻿using System.Collections.Generic;
using UnityEngine;

public interface IOrigin
{
	/// <summary>
	/// Calculates the origin for given cities.
	/// </summary>
	/// <returns>The origin.</returns>
	/// <param name="cities">Cities.</param>
	Vector3 GetOrigin(List<ICity> cities);

	/// <summary>
	/// Gets the full angle factor of this IOrigin in [0, 1] range.
	/// Origin having cities around in all directions should return 1,
	/// another origin being on the edge would return 0.5, etc...
	/// </summary>
	/// <returns>The angle factor.</returns>
	float FullAngleFactor();
}
