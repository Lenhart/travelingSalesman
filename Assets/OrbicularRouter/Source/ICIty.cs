﻿using UnityEngine;

public interface ICity
{
	Vector3 GetPosition();
}
