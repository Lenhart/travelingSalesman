﻿using UnityEngine;

public static class Util
{
	/// <summary>
	/// Calculates angle of the city for given origin.
	/// </summary>
	/// <returns>The angle.</returns>
	/// <param name="city">City.</param>
	/// <param name="origin">Origin.</param>
	public static float Angle1D(this ICity city, Vector3 origin)
	{
		var positionByOrigin = city.GetPosition() - origin;
		var angle =Mathf.Atan2(positionByOrigin.z, positionByOrigin.x) * Mathf.Rad2Deg;
		return ClampAngle(angle);
	}

	/// <summary>
	/// Calculates angle of the city for default origin (0, 0, 0).
	/// </summary>
	/// <returns>The angle.</returns>
	/// <param name="city">City.</param>
	public static float Angle1D(this ICity city)
	{
		return city.Angle1D(Vector3.zero);
	}

	/// <summary>
	/// Clamp angle to [0, 360) range.
	/// </summary>
	/// <returns>Clamped angle.</returns>
	/// <param name="angle">Angle.</param>
	public static float ClampAngle(float angle) {
		return (angle + 360f) % 360f;
    }
}
