﻿using System;
using System.Collections.Generic;

/// <summary>
/// Radix sort for float values. Values have to be in [0, 1) range.
/// Implementation is MSD (most significat digit first) and unstable.
/// It could easily be improved to become stable though, with minor performance hit.
/// Worst-case complexity is O(7n) since floats have 7 precision digits.
/// </summary>
public class Float01RadixSort<T>
{
	private class SortElement
	{
		public T Element { get; set; }
		public string Key { get; set; }
		// TODO Optimize: Allocate array SortElement[Count] and use index as Next instead of many small allocations
		public SortElement Next { get; set; }
		public SortElement(T element, string key)
		{
			Element = element;
			Key = key;
		}
	}

	private class RadixNode
	{
		public int Depth { get; set; }
		public RadixNode[] Nodes { get; set; }
		public SortElement Elements { get; set; }

		public RadixNode(int depth = 0)
		{
			Depth = depth;
		}

		public void Insert(SortElement element)
		{
			// Is this the first element entering this node? Don't waste time creating deep structure,
			// this may be the only element in this node.
			if (Nodes == null && Elements == null)
			{
				Elements = element;
				return;
			}
			// Does element trully belong to this node?
			if (element.Key.Length == Depth)
			{
				if (Elements == null)
				{
					Elements = element;
				}
				else
				{
					// Is there other element that should actually go deeper?
					if (Elements.Key.Length != Depth)
					{
						var deeperElement = Elements;
						Elements = element;
						Insert(deeperElement);
					}
					else
					{
						// Following line makes algorithm unstable, because new element is added to
						// the beginning of the list, but for TSP it's not important
						element.Next = Elements;
						Elements = element;
					}
				}
			}
			else
			{
				if (Nodes == null)
				{
					Nodes = new RadixNode[10];
					// Is there other element that should actually go deeper?
					if (Elements != null && Elements.Key.Length != Depth)
					{
						var deeperElement = Elements;
						Elements = null;
						Insert(deeperElement);
					}
				}
				int key = element.Key[Depth] - '0';
				RadixNode childNode = Nodes[key];
				if (childNode == null)
				{
					childNode = new RadixNode(Depth + 1);
					Nodes[key] = childNode;
				}
				childNode.Insert(element);
			}
		}

		public void TraversePreOrder(List<T> list)
		{
			SortElement element = Elements;
			while (element != null)
			{
				// TODO Provide second criteria for sorting, passing the last element from
				// the list to improve total route cost
				list.Add(element.Element);
				element = element.Next;
			}
			if (Nodes != null)
			{
				for (int i = 0; i < 10; ++i)
				{
					var childNode = Nodes[i];
					if (childNode != null)
					{
						childNode.TraversePreOrder(list);
					}
				}
			}
		}
	}

	/// <summary>
	/// Sort the specified list using sortValueFunction to provide float value of list elements.
	/// The sortValueFunction must return values in [0, 1) range!
	/// Worst-case complexity is O(7n).
	/// </summary>
	/// <param name="list">List to be sorted.</param>
	/// <param name="sortValueFunction">Function returning float value in [0, 1) range for each list element.</param>
	public static void Sort(List<T> list, Func<T, float> sortValueFunction)
	{
		if (list == null || list.Count <= 1)
		{
			// There's nothing to be done
			return;
		}

		RadixNode root = new RadixNode();
		int count = list.Count;
		for (int i = 0; i < count; ++i)
		{
			T element = list[i];
			float value = sortValueFunction(element);
			if (value < 0f || value >= 1f) throw new ArgumentException("Trying to sort value out of [0, 1) range (" + value + ")");
			string key = value.ToString("0.0######").Substring(2);
			root.Insert(new SortElement(element, key));
		}

		list.Clear();
		root.TraversePreOrder(list);
	}
}
