﻿using System;
using System.Linq;
using UnityEngine;

/// <summary>
/// Context map implemented by array of floats.
/// </summary>
public class ContextMap {
	private const float OCCUPACY_MIN_INTENSITY_RELAX = 1.05f;

	private float[] map;
	private int resolution;
	private float maxPosition;

	public ContextMap(int resolution = 32, float maxPosition = 360f) {
		if (resolution <= 1) throw new ArgumentException("Must be more than 1", "resolution");
		if (maxPosition <= 0) throw new ArgumentException("Must be more than 0", "maxPosition");
		this.resolution = resolution;
		this.maxPosition = maxPosition;
		map = new float[resolution];
	}

	/// <summary>
	/// Occupy the specified position with given value and spread it in given width.
	/// </summary>
	/// <param name="position">Position.</param>
	/// <param name="value">Value.</param>
	/// <param name="width">Width.</param>
	public void Occupy(float position, float value, float width) {
		var halfWidth = width / 2f;
		int start = PositionToIndex(position - halfWidth);
		int end = PositionToIndex(position + halfWidth);
		for (int i = start; ; i = (i + 1) % resolution) {
			map[i] += value;
			if (i == end) {
				break;
			}
		}
	}

	/// <summary>
	/// Finds the position, width and intensity of the lowest value in the map.
	/// If there are several places where this value is found, the widest one
	/// will be returned.
	/// Position is packed to x component, width is packed to y component and
	/// intensity is packed to z component of Vector3.
	/// </summary>
	/// <returns>The breach.</returns>
	public Vector3 FindBreach() {
		float relaxedMin = map.Min() * OCCUPACY_MIN_INTENSITY_RELAX;
		int length = 0;
		int started = -1;
		int bestLength = 0;
		int bestStarted = 0;
		for (int i = 0; i < resolution * 2; ++i) {
			int realIndex = i % resolution;
			if (realIndex == started) break;
			if (map[realIndex] <= relaxedMin) {
				if (length == 0) {
					started = realIndex;
				}
				++length;
			} else if (length > bestLength) {
				bestLength = length;
				bestStarted = started;
				length = 0;
			}
		}
		if (length > bestLength) {
			bestLength = length;
			bestStarted = started;
			length = 0;
		}
		var position = IndexToPosition(bestStarted + bestLength / 2);
		var width = bestLength / (float)resolution * maxPosition;
		var intensity = map[bestStarted];
		return new Vector3(position, width, intensity);
	}

	private int PositionToIndex(float position) {
		position = (position + maxPosition) % maxPosition;
		return (int) (position / maxPosition * resolution);
	}

	private float IndexToPosition(int index) {
		index = (index + resolution) % resolution;
		return index * maxPosition / resolution;
	}

	/// <summary>
	/// Fades (scales) map with given factor.
	/// </summary>
	/// <param name="factor">Factor.</param>
	public void Fade(float factor = 0.9f) {
		for (int i = 0; i < resolution; ++i) {
			map[i] *= factor;
		}
	}

	/// <summary>
	/// Reset this instance.
	/// </summary>
	public void Reset() {
		Fade(0f);
	}
}