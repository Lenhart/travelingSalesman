﻿using System;
using UnityEngine;

/// <summary>
/// Context map implemented by bits.
/// </summary>
public class BitContextMap
{
	private long map;
	private int resolution;
	private float maxPosition;

	public BitContextMap(int resolution = 32, float maxPosition = 360f)
	{
		if (resolution < 1 || resolution > 64) throw new ArgumentException("Set to [1, 64] range", "resolution");
		if (maxPosition <= 0) throw new ArgumentException("Set to positive value", "maxPosition");

		this.resolution = resolution;
		this.maxPosition = maxPosition;
	}

	/// <summary>
	/// Calculates number of bits set in given value.
	/// </summary>
	/// <returns>Number of bits set.</returns>
	/// <param name="value">Value.</param>
	public static int CalculateBitsSet(long value)
	{
		value = value - ((value >> 1) & 0x5555555555555555);
		value = (value & 0x3333333333333333) + ((value >> 2) & 0x3333333333333333);
		value = (((value + (value >> 4)) & 0x0F0F0F0F0F0F0F0F) * 0x0101010101010101) >> 56;
		return (int)value;
	}

	/// <summary>
	/// Occupy the specified position.
	/// </summary>
	/// <param name="position">Position.</param>
	public void Occupy(float position)
	{
		float factor = Mathf.Clamp01(position / maxPosition);
		int bitPlace = (int)(factor * resolution);
		if (bitPlace >= resolution) bitPlace--;

		long mask = 0x01L << bitPlace;
		map = map | mask;
	}

	/// <summary>
	/// Gets number of bits set in the map.
	/// </summary>
	/// <returns>Number of bits set.</returns>
	public int GetBitsSet()
	{
		return CalculateBitsSet(map);
	}

	/// <summary>
	/// Gets the fill percentage of the map.
	/// </summary>
	/// <returns>The fill percentage in [0, 1] range.</returns>
	public float GetFillPercentage()
	{
		return GetBitsSet() / (float)resolution;
	}

	/// <summary>
	/// Clear the map.
	/// </summary>
	public void Clear()
	{
		map = 0L;
	}
}
