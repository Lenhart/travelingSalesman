﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Auto parameters orbicular router.
/// Finds radiuses by binary search.
/// Development discontinued.
/// </summary>
public class AutoParamsOrbicularRouter : AbstractRouterWithOrigin {
	private const int MAX_EQUALITY_COUNT = 3;
	public List<float> Radiuses { get; private set; }
	private List<Vector2> Ranges { get; set; }

	public AutoParamsOrbicularRouter(List<ICity> cities, ICityDistance distanceCalculator, IOrigin originCalculator)
		: base(cities, distanceCalculator, originCalculator) {
		Radiuses = new List<float>();
		Ranges = new List<Vector2>();
		Ranges.Add(new Vector2(0f, MaxEuclidDistanceFromOrigin + Mathf.Epsilon));
	}

	protected override List<ICity> Route() {
		while (Ranges.Count > 0) {
			Vector2 range = Ranges[0];
			Ranges.RemoveAt(0);
			float newRadius = FindRadiusInRange(range.x, range.y);
			if (newRadius >= 0f) {
				Ranges.Add(new Vector2(range.x, newRadius));
				Ranges.Add(new Vector2(newRadius, range.y));
				Radiuses = BuildRadiuses(newRadius);
			}
		}

		return GetResult(Radiuses);
	}

	private List<ICity> GetResult(List<float> radiuses) {
		List<ICity> result = null;
		try {
			OrbicularRouter orbicular = new OrbicularRouter(Cities, DistanceCalculator, OriginCalculator, radiuses);
			result = orbicular.BuildRoute();
		} catch (Exception) { }
		return result;
	}

	public static float CalculateDistance(List<ICity> citiesForDistance, ICityDistance distanceCalculator)
	{
		var distance = 0f;
		var citiesCount = citiesForDistance.Count;
		for (int i = 0; i < citiesCount - 1; ++i)
		{
			distance += distanceCalculator.GetDistance(citiesForDistance[i].GetPosition(), citiesForDistance[i + 1].GetPosition());
		}
		if (citiesCount > 1)
		{
			distance += distanceCalculator.GetDistance(citiesForDistance[citiesCount - 1].GetPosition(), citiesForDistance[0].GetPosition());
		}
		return distance;
	}

	private float GetPrice(List<float> radiuses) {
		var result = GetResult(radiuses);
		return result != null ? CalculateDistance(result, DistanceCalculator) : float.PositiveInfinity;
	}

	private List<float> BuildRadiuses(float radius) {
		List<float> list = new List<float>(Radiuses);
		list.Add(radius);
		list.Sort();
		return list;
	}

	private float FindRadiusInRange(float low, float high, float? lowPriceOrNull = null, float? highPriceOrNull = null, int equalPriceCounter = 0) {
		float mid = (low + high) / 2f;
		float lowPrice;
		float highPrice;
		if (lowPriceOrNull.HasValue) {
			lowPrice = lowPriceOrNull.Value;
			highPrice = highPriceOrNull.Value;
		} else {
			lowPrice = GetPrice(BuildRadiuses(low));
			highPrice = GetPrice(BuildRadiuses(high));
			if (Mathf.Approximately(lowPrice, highPrice)) {
				// This range was added for radius found through equalPriceCounter
				return -1;
			}
		}
		List<float> midRadiuses = BuildRadiuses(mid);
		float midPrice = GetPrice(midRadiuses);

		if (midPrice > lowPrice && midPrice > highPrice) {
			return -1;
		}
		if (Mathf.Approximately(midPrice, lowPrice) || Mathf.Approximately(midPrice, highPrice)) {
			if (++equalPriceCounter >= MAX_EQUALITY_COUNT) {
				return mid;
			}
		}
		float radius = lowPrice <= highPrice ? FindRadiusInRange(low, mid, lowPrice, midPrice, equalPriceCounter) : FindRadiusInRange(mid, high, midPrice, highPrice, equalPriceCounter);
		if (radius < 0) {
			radius = lowPrice > highPrice ? FindRadiusInRange(low, mid, lowPrice, midPrice, equalPriceCounter) : FindRadiusInRange(mid, high, midPrice, highPrice, equalPriceCounter);
		}
		return radius;
	}
}
