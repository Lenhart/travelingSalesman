﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractRouterWithOrigin : AbstractRouter
{
	private const float MinimalMaxDistanceFromOrigin = 1f;

	protected IOrigin OriginCalculator { get; private set; }

	private bool originCalculated;
	private Vector3 origin;
	public Vector3 Origin
	{
		get
		{
			if (!originCalculated)
			{
				originCalculated = true;
				origin = OriginCalculator.GetOrigin(Cities);
			}
			return origin;
		}
	}

	private float maxDistanceFromOrigin = -1f;
	protected virtual float MaxEuclidDistanceFromOrigin
	{
		get
		{
			if (maxDistanceFromOrigin < 0)
			{
				var euclidDistanceCalculator = EuclidDistanceCalculator.GetInstance();
				for (int i = 0; i < CitiesCount; ++i)
				{
					var distance = euclidDistanceCalculator.GetDistance(Origin, Cities[i].GetPosition());
					if (distance > maxDistanceFromOrigin)
					{
						maxDistanceFromOrigin = distance;
					}
				}
				maxDistanceFromOrigin += Mathf.Epsilon;
				maxDistanceFromOrigin = Mathf.Max(maxDistanceFromOrigin, MinimalMaxDistanceFromOrigin);
			}
			return maxDistanceFromOrigin;
		}
	}

	public AbstractRouterWithOrigin(List<ICity> cities, ICityDistance distanceCalculator, IOrigin originCalculator)
		: base(cities, distanceCalculator)
	{
		if (originCalculator == null)
		{
			throw new ArgumentNullException("Give me originCalculator");
		}
		OriginCalculator = originCalculator;
	}

	protected float OriginDistance(ICity city)
	{
		var toCity = city.GetPosition();
		return DistanceCalculator.GetDistance(Origin, toCity);
	}
}
