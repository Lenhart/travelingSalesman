using System;
using System.Collections.Generic;
using UnityEngine;

public class OrbicularRouter : AbstractRouterWithOrigin {
	public const float OCCUPACY_MIN_RADIUS = 1f;
	public const float OCCUPACY_MAX_RADIUS = 10f;
	public const float OCCUPACY_MIN_ANGLE = 60f;
	public const float OCCUPACY_MAX_ANGLE = 15f;
	public const float OCCUPACY_INTENSITY = 0.5f;

	private class Circle {
		public float Radius { get; private set; }
		public List<ICity> Cities { get; private set; }
		private Vector3 Origin { get; set; }
		public Vector2 InnerBreach { get; set; }
		public Vector2 OuterBreach { get; set; }
		public List<ICity> InOutCities { get; set; }
		public List<ICity> OutInCities { get; set; }

		public Circle(float radius, Vector3 origin) {
			Radius = radius;
			Cities = new List<ICity>();
			Origin = origin;
		}

		public void Add(ICity city) {
			Cities.Add(city);
		}
	}

	private List<Circle> circles;
	private List<float> radiuses;

	public OrbicularRouter(List<ICity> cities, ICityDistance distanceCalculator, IOrigin originCalculator, List<float> radiuses)
		: base(cities, distanceCalculator, originCalculator)
	{
		if (radiuses != null) {
			this.radiuses = new List<float>(radiuses);
		} else {
			this.radiuses = new List<float>(1);
		}
		float lastRadius = 0f;
		for (int i = 0; i < this.radiuses.Count; ++i) {
			if (this.radiuses[i] < lastRadius) {
				throw new ArgumentException(string.Format("Invalid radius: {0} on index: {1}", this.radiuses[i], i));
			}
			lastRadius = this.radiuses[i];
		}
		if (lastRadius < float.MaxValue) {
			this.radiuses.Add(float.MaxValue);
		}
	}

	protected override List<ICity> Route() {
		BuildCircles();
		PopulateCircleCities();
		PopulateCircleBreaches();
		PopulateCircleCitySplits();
		List<ICity> route = JoinCircles();
		route = Linear2Opt(route);
		return route;
	}

	private void BuildCircles() {
		circles = new List<Circle>();
		for (int i = 0; i < radiuses.Count; ++i) {
			circles.Add(new Circle(radiuses[i], Origin));
		}
	}

	private void PopulateCircleCities() {
		for (int i = 0; i < CitiesCount; ++i) {
			ICity city = Cities[i];
			int index = GetCircleIndex(city);
			circles[index].Add(city);
		}
	}

	private int GetCircleIndex(ICity city) {
		float distance = OriginDistance(city);
		int searchIndex = radiuses.BinarySearch(distance);
		if (searchIndex >= 0) {
			return searchIndex;
		} else {
			searchIndex = ~searchIndex;
			if (searchIndex == 0) {
				return searchIndex;
			} else if (searchIndex == radiuses.Count) {
				return radiuses.Count - 1;
			} else {
				return searchIndex;
            }
        }
    }

	private void PopulateCircleBreaches() {
		var map = new ContextMap();
		var nextMap = new ContextMap();
		var circlesCount = circles.Count;
		var previousCircle = circles[circlesCount - 1];
		FillContextMap(map, null, previousCircle.Cities);
		var halfFloatMax = float.MaxValue / 2f;

		for (int i = circlesCount - 2; i >= 0; --i) {
			// Fill maps with circle data
			var currentCircle = circles[i];
			FillContextMap(map, nextMap, currentCircle.Cities);

			// Store breach data
			var breach = map.FindBreach();
			previousCircle.InnerBreach = breach;
			currentCircle.OuterBreach = breach;
			// Blacklist the breach only in case there are no cities (real breach)
			if (breach.z <= 0.01f) {
				// Blacklist this breach for following ring
				// Blacklist at least 3 more entries, but at least one entry has to stay. We're using
				// context map of resolution 32, hence the values.
				var blacklistWidth = Mathf.Min(breach.y + 70f, 335f);
				nextMap.Occupy(breach.x, halfFloatMax, blacklistWidth);
			}

			// Prepare for next iteration
			var temp = map;
			map = nextMap;
			nextMap = temp;
			nextMap.Reset();
			previousCircle = currentCircle;
		}
		// Make sure breach of first circle is easily accessed through any property
		var firstCircle = circles[0];
		firstCircle.InnerBreach = firstCircle.OuterBreach;
	}

	private void FillContextMap(ContextMap map1, ContextMap map2, List<ICity> cities) {
		for (int i = 0; i < cities.Count; ++i) {
			var city = cities[i];
			var angle = city.Angle1D(Origin);
			var lerpFactor = Mathf.Clamp01(Mathf.InverseLerp(OCCUPACY_MIN_RADIUS, OCCUPACY_MAX_RADIUS, OriginDistance(city)));
			var width = Mathf.Lerp(OCCUPACY_MIN_ANGLE, OCCUPACY_MAX_ANGLE, lerpFactor);
			map1.Occupy(angle, OCCUPACY_INTENSITY, width);
			if (map2 != null) {
				map2.Occupy(angle, OCCUPACY_INTENSITY, width);
			}
		}
	}

	private void PopulateCircleCitySplits() {
		var count = circles.Count;

		// Process inner ring
		var circle = circles[0];
		circle.InOutCities = circle.Cities;
		if (count == 1) return;

		// Process outer ring
		circle = circles[count - 1];
		circle.InOutCities = circle.Cities;

		// Process all other rings
		for (int i = 1; i < count - 1; ++i) {
			circle = circles[i];
			var inOutCities = new List<ICity>();
			var outInCities = new List<ICity>();
			var innerBreach = circle.InnerBreach;
			var outerBreach = circle.OuterBreach;

			var cities = circle.Cities;
			for (int j = 0; j < cities.Count; ++j) {
				var city = cities[j];
				var angle = city.Angle1D(Origin);
				if ((innerBreach.x < outerBreach.x && angle > innerBreach.x && angle < outerBreach.x)
				    || (innerBreach.x > outerBreach.x && (angle > innerBreach.x || angle < outerBreach.x))) {
					inOutCities.Add(city);
				} else {
					outInCities.Add(city);
				}
			}

			circle.InOutCities = inOutCities;
			circle.OutInCities = outInCities;
		}
	}

	private List<ICity> JoinCircles() {
		List<ICity> cities = new List<ICity>(CitiesCount);
		int circlesCount = circles.Count;

		bool reverseSort = AddCities(cities, circles[0], true, false);
		if (circlesCount == 1) return cities;

		for (int i = 1; i < circlesCount - 1; ++i) {
			reverseSort = AddCities(cities, circles[i], i % 2 == 0, reverseSort);
		}
		reverseSort = AddCities(cities, circles[circlesCount - 1], true, reverseSort);
		for (int i = circlesCount - 2; i >= 1; --i) {
			reverseSort = AddCities(cities, circles[i], i % 2 == 1, reverseSort);
		}

		return cities;
	}

	private bool AddCities(List<ICity> cities, Circle circle, bool inOut, bool reverseSort) {
		List<ICity> addition;
		Vector2 breach;
		if (inOut) {
			addition = circle.InOutCities;
			breach = circle.InnerBreach;
		}
		else {
			addition = circle.OutInCities;
			breach = circle.OuterBreach;
		}
		if (addition.Count > 0) {
			Sort(addition, breach.x, reverseSort);
			cities.AddRange(addition);
		}
		return !reverseSort;
	}

	private void Sort(List<ICity> cities, float positiveOffset, bool reverse) {
		Float01RadixSort<ICity>.Sort(cities, (city) => Util.ClampAngle(city.Angle1D(Origin) - positiveOffset) / 360f);
		if (reverse) {
			// TODO implement through calculation in sort
			cities.Reverse();
		}
	}

	private List<ICity> Linear2Opt(List<ICity> route) {
		int count = route.Count;
		for (int i = 0; i < count; ++i) {
			int a = i;
			int b = (i + 1) % count;
			int c = (i + 2) % count;
			int d = (i + 3) % count;
			float costABCD = DistanceCalculator.GetDistance(route[a].GetPosition(), route[b].GetPosition()) + DistanceCalculator.GetDistance(route[c].GetPosition(), route[d].GetPosition());
			float costACBD = DistanceCalculator.GetDistance(route[a].GetPosition(), route[c].GetPosition()) + DistanceCalculator.GetDistance(route[b].GetPosition(), route[d].GetPosition());
			if (costABCD > costACBD) {
				route.Insert(b, route[c]);
				// C is now at position of D, but in general case D could be first element in route
				route.RemoveAt(c + 1);
			}
		}
		return route;
	}
}
