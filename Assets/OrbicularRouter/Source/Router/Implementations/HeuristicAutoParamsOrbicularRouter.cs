using System;
using System.Collections.Generic;
using UnityEngine;

// TODO Consider tweaking methods
// - calculateStepSize()
// - calculateRingThreshold(float, float) and
// - calculatePathPrice(float, float, int)
// Check if it works good for more and/or less dense cities
public class HeuristicAutoParamsOrbicularRouter : AbstractRouterWithOrigin {
	private const float ROUNDING_WEIGHT = 0.7f;

	public List<float> Radiuses { get; set; }

	public HeuristicAutoParamsOrbicularRouter(List<ICity> cities, ICityDistance distanceCalculator, IOrigin originCalculator)
		: base(cities, distanceCalculator, originCalculator) { }

	protected override List<ICity> Route() {
		if (CitiesCount > 0) {
			float stepSize = calculateStepSize();
			var cityGroups = groupCities(stepSize);
			Radiuses = calculateRadiuses(cityGroups, stepSize);
		}

		return new OrbicularRouter(Cities, DistanceCalculator, OriginCalculator, Radiuses).BuildRoute();
	}

	private float calculateStepSize() {
		float totalArea = MaxEuclidDistanceFromOrigin * MaxEuclidDistanceFromOrigin;
		float singleCityArea = totalArea / CitiesCount;
		return Mathf.Sqrt(singleCityArea) * 3f * Mathf.Sqrt(OriginCalculator.FullAngleFactor());
	}

    private List<List<ICity>> groupCities(float stepSize) {
		int groups = Mathf.CeilToInt(MaxEuclidDistanceFromOrigin / stepSize) + 1;
		List<List<ICity>> cityGroups = new List<List<ICity>>(groups);
		for (int i = 0; i < groups; ++i) {
			cityGroups.Add(new List<ICity>());
		}
		for (int i = 0; i < CitiesCount; ++i) {
			var city = Cities[i];
			int index = Mathf.FloorToInt(EuclidDistance(city) / stepSize);
			cityGroups[index].Add(city);
		}
		return cityGroups;
	}

	private float EuclidDistance(ICity city)
	{
		var toCity = city.GetPosition();
		return EuclidDistanceCalculator.GetInstance().GetDistance(Origin, toCity);
	}

	private List<float> calculateRadiuses(List<List<ICity>> cityGroups, float stepSize) {
		List<float> radiuses = new List<float>();
		var groupsCount = cityGroups.Count;
		float previousRadius = groupsCount * stepSize;
		int citiesInside = 0;
		var map = new BitContextMap(16);
		for (int i = groupsCount - 1; i > 0; --i) {
			var group = cityGroups[i];
			var currentRadius = i * stepSize;
			citiesInside += group.Count;
			PopulateMap(map, group, Origin);
			var mapFillPercentage = map.GetFillPercentage();
			float pathPrice = calculatePathPrice(currentRadius, previousRadius, citiesInside, mapFillPercentage);
			float pathThreshold = calculateRingThreshold(currentRadius, previousRadius, mapFillPercentage);
			if (pathPrice > pathThreshold) {
				radiuses.Insert(0, currentRadius);
				previousRadius = currentRadius;
				citiesInside = 0;
				map.Clear();
			}
		}
		return radiuses;
	}

	private void PopulateMap(BitContextMap map, List<ICity> cities, Vector3 origin)
	{
		for (int i = 0; i < cities.Count; i++)
		{
			var city = cities[i];
			map.Occupy(city.Angle1D(origin));
		}
	}

	private float calculateRingThreshold(float innerRadius, float outerRadius, float ringFillPercentage) {
		float innerRing = 2f * innerRadius * Mathf.PI;
		float outerRing = 2f * outerRadius * Mathf.PI;
		return (innerRing + outerRing) * ringFillPercentage;
	}

	private float calculatePathPrice(float innerRadius, float outerRadius, int citiesCount, float ringFillPercentage) {
		int paths = citiesCount - 1;
		if (paths < 1) {
			return 0f;
		}
		float innerRing = 2f * innerRadius * Mathf.PI * ringFillPercentage;
		float outerRing = 2f * outerRadius * Mathf.PI * ringFillPercentage;
		float cityStepArc = (outerRing - innerRing) / paths;
		float radiusDiff = outerRadius - innerRadius;
		float singlePath = Mathf.Sqrt(radiusDiff * radiusDiff + cityStepArc * cityStepArc);
		singlePath *= ROUNDING_WEIGHT;
		return innerRing + paths * singlePath;
	}
}
