﻿using System;
using System.Collections.Generic;
using System.Linq;

public abstract class AbstractRouter {
	protected List<ICity> Cities { get; private set; }
	protected int CitiesCount
	{
		get
		{
			return Cities != null ? Cities.Count : 0;
		}
	}

	protected ICityDistance DistanceCalculator { get; private set; }

	protected AbstractRouter(List<ICity> cities, ICityDistance distanceCalculator)
	{
		if (cities == null || cities.Count < 1)
		{
			throw new ArgumentException("Feed me with some cities for routing");
		}
		if (distanceCalculator == null)
		{
			throw new ArgumentNullException("Give me distanceCalculator");
		}
		Cities = cities;
		DistanceCalculator = distanceCalculator;
	}

	protected abstract List<ICity> Route();

	public List<ICity> BuildRoute() {
		var route = Route();
		
		if (route == null || route.Count != CitiesCount)
		{
			throw new InvalidOperationException("Route is missing or has added some cities");
		}
		if (route.Intersect(Cities).Count() != CitiesCount) {
			throw new InvalidOperationException("Route visits some of the cities more than once");
		}
		
		return route;
	}
}
