using System.Collections.Generic;

/// <summary>
/// Brute force router.
/// Note, if you want to use this router your implementation of ICity must also implement System.IComparable.
/// </summary>
public class BruteForceRouter : AbstractRouter {
	public BruteForceRouter(List<ICity> cities, ICityDistance distanceCalculator)
		: base(cities, distanceCalculator) {
	}

	protected override List<ICity> Route() {
		ICity[] permutation = GetFirstPermutation();
		ICity[] candidate = (ICity[]) permutation.Clone();
		float distance = CalculatePermutationDistance(candidate);
		int permCount = 1;
		while (NextPermutation(permutation)) {
			float currentDistance = CalculatePermutationDistance(permutation);
			if (IsBetter(currentDistance, distance)) {
				candidate = (ICity[]) permutation.Clone();
				distance = currentDistance;
			}
			++permCount;
		}
		UnityEngine.Debug.Log("Permutations tested: " + permCount);

		return new List<ICity>(candidate);
	}

	protected virtual bool IsBetter(float currentDistance, float distance) {
		return currentDistance < distance;
	}

	protected float CalculatePermutationDistance(ICity[] permutation) {
		float distance = 0;
		for (int i = 0; i < permutation.Length - 1; ++i) {
			distance += DistanceCalculator.GetDistance(permutation[i].GetPosition(), permutation[i + 1].GetPosition());
		}
		if (permutation.Length > 1) {
			distance += DistanceCalculator.GetDistance(permutation[permutation.Length - 1].GetPosition(), permutation[0].GetPosition());
		}
		return distance;
	}

	protected ICity[] GetFirstPermutation() {
		List<ICity> cityList = new List<ICity>(Cities);
		cityList.Sort();
		return cityList.ToArray();
	}

	/**
	 * From http://stackoverflow.com/questions/11208446/generating-permutations-of-a-set-most-efficiently
	 * Then just altered a bit to work with ICity.
	 * Expects to start from naturally ordered permutation, ie: { 1, 2, 3, 4 }.
	 */
	protected static bool NextPermutation(ICity[] numList) {
		/*
         Knuths
         1. Find the largest index j such that a[j] < a[j + 1]. If no such index exists, the permutation is the last permutation.
         2. Find the largest index l such that a[j] < a[l]. Since j + 1 is such an index, l is well defined and satisfies j < l.
         3. Swap a[j] with a[l].
         4. Reverse the sequence from a[j + 1] up to and including the final element a[n].
         */
		var largestIndex = -1;
		for (var i = numList.Length - 2; i >= 0; i--)
		{
			if (numList[i].GetHashCode() < numList[i + 1].GetHashCode()) {
				largestIndex = i;
				break;
			}
		}

		if (largestIndex < 0) return false;

		var largestIndex2 = -1;
		for (var i = numList.Length - 1 ; i >= 0; i--) {
			if (numList[largestIndex].GetHashCode() < numList[i].GetHashCode()) {
				largestIndex2 = i;
				break;
			}
		}

		var tmp = numList[largestIndex];
		numList[largestIndex] = numList[largestIndex2];
		numList[largestIndex2] = tmp;

		for (int i = largestIndex + 1, j = numList.Length - 1; i < j; i++, j--) {
			tmp = numList[i];
			numList[i] = numList[j];
			numList[j] = tmp;
		}

		return true;
	}
}
