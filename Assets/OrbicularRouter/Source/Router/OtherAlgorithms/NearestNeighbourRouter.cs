﻿using System.Collections.Generic;

public class NearestNeighbourRouter : AbstractRouter {
	public NearestNeighbourRouter(List<ICity> cities, ICityDistance distanceCalculator)
		: base(cities, distanceCalculator)
	{

	}

	protected override List<ICity> Route()
	{
		var citiesLeft = new List<ICity>(Cities);
		var result = new List<ICity>();

		int randomIndex = RandomIndex();
		ICity lastCity = citiesLeft[randomIndex];
		result.Add(lastCity);
		citiesLeft.RemoveAt(randomIndex);

		while (citiesLeft.Count > 0) {
			int nextIndex = FindNextIndex(lastCity, citiesLeft);
			lastCity = citiesLeft[nextIndex];
			result.Add(lastCity);
			citiesLeft.RemoveAt(nextIndex);
		}

		return result;
	}

	private int RandomIndex(int maxNonInclusive = -1) {
		int max = maxNonInclusive > 0 ? maxNonInclusive : CitiesCount;
		return (int) (max * UnityEngine.Random.value);
	}

	protected virtual int FindNextIndex(ICity city, List<ICity> others) {
		return ClosestCityIndex(city, others);
	}

	protected int ClosestCityIndex(ICity city, List<ICity> others) {
		int index = 0;
		float distance = DistanceCalculator.GetDistance(city.GetPosition(), others[0].GetPosition());
		for (int i = 1; i < others.Count; ++i) {
			float currentDistance = DistanceCalculator.GetDistance(city.GetPosition(), others[i].GetPosition());
			if (currentDistance < distance) {
				distance = currentDistance;
				index = i;
			}
		}
		return index;
	}
}
