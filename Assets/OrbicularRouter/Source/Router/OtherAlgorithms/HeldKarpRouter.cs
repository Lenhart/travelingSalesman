using System.Collections.Generic;
using System.Linq;
using System.Text;

public class HeldKarpRouter : AbstractRouter {
	private class RoutePart {
		public int TargetCityIndex { get; private set; }
		public List<int> SubrouteCityIndices { get; private set; }
		public float TotalDistance { get; set; }
		public int ViaCityIndex { get; set; }

		public RoutePart(int targetCityIndex, List<int> subrouteCityIndices = null) {
			TargetCityIndex = targetCityIndex;
			SubrouteCityIndices = subrouteCityIndices ?? new List<int>();
			TotalDistance = -1f;
			ViaCityIndex = -1;
		}

		public override bool Equals(object obj) {
			RoutePart that = (RoutePart) obj;
			return (TargetCityIndex == that.TargetCityIndex)
				&& (SubrouteCityIndices.SequenceEqual(that.SubrouteCityIndices));
		}

		public override int GetHashCode() {
			return TargetCityIndex;
		}

		public string SubHash() {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < SubrouteCityIndices.Count; ++i) {
				sb.Append(SubrouteCityIndices[i].ToString()).Append("-");
			}
			return sb.ToString();
		}
	}

	private List<RoutePart> routeParts;
	private Dictionary<int, Dictionary<int, Dictionary<string, RoutePart>>> routePartsMap;

	public HeldKarpRouter(List<ICity> cities, ICityDistance distanceCalculator)
		: base(cities, distanceCalculator) {
		routeParts = new List<RoutePart>();
		routePartsMap = new Dictionary<int, Dictionary<int, Dictionary<string, RoutePart>>>();
	}

	protected override List<ICity> Route() {
		var route = new List<ICity>();
		if (CitiesCount > 0) {
			BuildRouteParts();
			SolveRouteParts();
			TrackBack(route);
		}
		return route;
	}

	private List<int> BuildCityIndices() {
		List<int> cityIndices = new List<int>();
		for (int cityIndex = 1; cityIndex < CitiesCount; ++cityIndex) {
			cityIndices.Add(cityIndex);
		}
		return cityIndices;
	}

	private void AddRoutePart(int targetCityIndex, List<int> subrouteCityIndices = null) {
		RoutePart part = new RoutePart(targetCityIndex, subrouteCityIndices);
		routeParts.Add(part);

		if (!routePartsMap.ContainsKey(targetCityIndex)) {
			routePartsMap.Add(targetCityIndex, new Dictionary<int, Dictionary<string, RoutePart>>());
		}
		var partMapsMap = routePartsMap[targetCityIndex];
		int subcitiesCount = part.SubrouteCityIndices.Count;
		if (!partMapsMap.ContainsKey(subcitiesCount)) {
			partMapsMap.Add(subcitiesCount, new Dictionary<string, RoutePart>());
		}
		partMapsMap[subcitiesCount].Add(part.SubHash(), part);
	}

	private RoutePart FindRoutePart(int targetCityIndex, List<int> subrouteCityIndices) {
		RoutePart routePart = null;
		var partMapsMap = routePartsMap[targetCityIndex];
		int subcitiesCount = subrouteCityIndices.Count;
		if (partMapsMap.ContainsKey(subcitiesCount)) {
			var map = partMapsMap[subcitiesCount];
			RoutePart subroutePartPrototype = new RoutePart(targetCityIndex, subrouteCityIndices);
			string subHash = subroutePartPrototype.SubHash();
			if (map.ContainsKey(subHash)) {
				routePart = map[subroutePartPrototype.SubHash()];
			}
		}
		return routePart;
	}

	private void BuildRouteParts() {
		List<int> cityIndices = BuildCityIndices();
		foreach (int cityIndex in cityIndices) {
			AddRoutePart(cityIndex);
		}
		int indexToExpand = 0;
		while (indexToExpand < routeParts.Count) {
			RoutePart expanding = routeParts[indexToExpand++];
			List<int> cities = new List<int>(expanding.SubrouteCityIndices);
			cities.Add(expanding.TargetCityIndex);
			cities.Sort();
			foreach (int cityIndex in cityIndices) {
				if (!cities.Contains(cityIndex) && (FindRoutePart(cityIndex, cities) == null)) {
					AddRoutePart(cityIndex, cities);
				}
			}
		}
		AddRoutePart(0, cityIndices);
	}

	private void SolveRouteParts() {
		Dictionary<int, float> viaCityCostMap = new Dictionary<int, float>();
		for (int solvingPartIndex = 0; solvingPartIndex < routeParts.Count; ++solvingPartIndex) {
			RoutePart solvingPart = routeParts[solvingPartIndex];
			if (solvingPart.SubrouteCityIndices.Count <= 0) {
				solvingPart.ViaCityIndex = 0;
				solvingPart.TotalDistance = DistanceCalculator.GetDistance(Cities[0].GetPosition(), Cities[solvingPart.TargetCityIndex].GetPosition());
			} else {
				viaCityCostMap.Clear();
				for (int i = 0; i < solvingPart.SubrouteCityIndices.Count; ++i) {
					int viaCityIndex = solvingPart.SubrouteCityIndices[i];
					float viaToTargetCityCost = DistanceCalculator.GetDistance(Cities[viaCityIndex].GetPosition(), Cities[solvingPart.TargetCityIndex].GetPosition());

					List<int> subrouteCityIndices = new List<int>(solvingPart.SubrouteCityIndices);
					subrouteCityIndices.Remove(viaCityIndex);
					RoutePart subroutePart = FindRoutePart(viaCityIndex, subrouteCityIndices);
					float subRouteCost = subroutePart.TotalDistance;

					viaCityCostMap.Add(viaCityIndex, viaToTargetCityCost + subRouteCost);
				}
				int bestViaCityIndex = viaCityCostMap.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
				solvingPart.ViaCityIndex = bestViaCityIndex;
				solvingPart.TotalDistance = viaCityCostMap[bestViaCityIndex];
			}
		}
	}

	private void TrackBack(List<ICity> route) {
		RoutePart part = routeParts[routeParts.Count - 1];
		List<int> subrouteCityIndices = new List<int>(part.SubrouteCityIndices);
		while (part.SubrouteCityIndices.Count > 0) {
			route.Insert(0, Cities[part.ViaCityIndex]);
			subrouteCityIndices.Remove(part.ViaCityIndex);
			part = FindRoutePart(part.ViaCityIndex, subrouteCityIndices);
		}
		route.Insert(0, Cities[0]);
	}
}
